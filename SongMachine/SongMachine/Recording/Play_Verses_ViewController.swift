//
//  Play_Verses_ViewController.swift
//  SongMachine
//
//  Created by SAM on 21/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit
import AVFoundation

protocol Play_Verses_Protocol{
    func addRecording(songDataDict : Dictionary<String,Any>)
    func goBack()
}

class Play_Verses_ViewController: UIViewController {
    var semaphore = DispatchSemaphore (value: 0)

    @IBOutlet var titleLabel: UILabel!
    var url: URL?
    var urlDict: Dictionary<String,Any>?
    var verseTitle: String!
    var playVC: Verses_ViewController!
    @IBOutlet weak var playButton: UIButton!
    var audioArr: Array<Any>!
    var delegate:RecordingDataDelegate?
    @IBOutlet weak var toolbar: UIToolbar!
    var songDataDict: Dictionary<String,Any>?
    @IBOutlet var audioView: AudioVisualizerView!
    var addRecodingDelegate : Play_Verses_Protocol?
    var player: AVAudioPlayer?
    let label = UILabel()
    var screenTitle = ""
    var toggleSwitch = true
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
        self.playButton.layer.cornerRadius = 0.5 * playButton.bounds.size.width
        createToolbar()
        label.text = screenTitle
        //let titlelbl = verseTitle!.capitalized
        //titleLabel.text = "Record \(String(describing: titlelbl))"
        let file = try! AVAudioFile(forReading: url!)
        let format = AVAudioFormat(commonFormat: .pcmFormatFloat32, sampleRate: file.fileFormat.sampleRate, channels: file.fileFormat.channelCount, interleaved: false)
        print(file.fileFormat.channelCount)
        let buf = AVAudioPCMBuffer(pcmFormat: format!, frameCapacity: UInt32(file.length))
        try! file.read(into: buf!)
            //
                    // this makes a copy, you might not want that
        readFile.arrayFloatValues = Array(UnsafeBufferPointer(start: buf?.floatChannelData?[0], count:Int(buf!.frameLength)))
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    func createToolbar(){
       
        toolbar.sizeToFit()
        toolbar.backgroundColor = UIColor(r: 35, g: 35, b: 52)
        toolbar.barTintColor = UIColor(r: 35, g: 35, b: 52)
         let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
         let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(DoneTapped))
         label.textColor = UIColor.white
         label.backgroundColor = UIColor.clear
         let labelAsBarButtonItem = UIBarButtonItem(customView: label)
         
         let btnCancel = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(closeTapped))
         
         
         btnDone.tintColor = UIColor(r: 250, g: 112, b: 154)
          
         btnCancel.tintColor = UIColor(r: 250, g: 112, b: 154)
        toolbar.setItems([btnCancel,space,labelAsBarButtonItem,space,btnDone], animated: true)
        toolbar.isUserInteractionEnabled = true
        
        
        
    }
    @objc func closeTapped(){
        
        //self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Verses_ViewController") as? Verses_ViewController
        delegate = vc
        
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        delegate?.updateAudioArray(updateSongDataDict: songDataDict!)
        
        vc!.songDataDict = songDataDict
        addRecodingDelegate?.addRecording(songDataDict: songDataDict!)
        
        
        //self.view.removeFromSuperview()
    }
    @objc func DoneTapped(){
        player?.stop()
        toggleSwitch = true
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Verses_ViewController") as? Verses_ViewController
        songDataDict![verseTitle] = url
        UserDefaults.standard.set(url, forKey: "\(String(describing: verseTitle))") //Bool
        
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        delegate?.updateAudioArray(updateSongDataDict: songDataDict!)

        vc!.songDataDict = songDataDict
        delegate = vc
        addRecodingDelegate?.addRecording(songDataDict: songDataDict!)
        self.view.removeFromSuperview()
    }
    @IBAction func playAudio(_ sender: Any) {
        if let url = self.url{
            playSound(url: url)
        }
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    func playSound(url: URL) {
          // let url = NSBundle.mainBundle().URLForResource("soundName", withExtension: "mp3")!
           do {
            
            if(toggleSwitch){
                player = try AVAudioPlayer(contentsOf: url)
                guard let player = player else { return }
                let session = AVAudioSession.sharedInstance()
                try session.setCategory(AVAudioSession.Category.playback)
                try session.setActive(true)
                player.prepareToPlay()
                player.play()
                toggleSwitch = false
                playButton.setImage(UIImage(imageLiteralResourceName: "pause"), for: .normal)
            }else{
                player?.stop()
                toggleSwitch = true
                playButton.setImage(UIImage(imageLiteralResourceName: "Play-1"), for: .normal)
            }

           } catch let error as NSError {
               print(error.description)
           }
       }
    
    @IBAction func incorrectTapped(_ sender: Any) {
        
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Verses_ViewController") as? Verses_ViewController
        delegate = vc
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        delegate?.updateAudioArray(updateSongDataDict: songDataDict!)
        vc!.songDataDict = songDataDict
        addRecodingDelegate?.goBack()
        
    }
    @IBAction func correctTapped(_ sender: Any) {
        player?.stop()
        toggleSwitch = true
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Verses_ViewController") as? Verses_ViewController
        songDataDict![verseTitle] = url
        UserDefaults.standard.set(url, forKey: "\(String(describing: verseTitle))") //Bool
        
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        delegate?.updateAudioArray(updateSongDataDict: songDataDict!)

        vc!.songDataDict = songDataDict
        delegate = vc
        addRecodingDelegate?.addRecording(songDataDict: songDataDict!)
        self.view.removeFromSuperview()
        
    }
    func callAddSongService(songDict:Dictionary<String,Any>?){
        if(songDict != nil){
            
            let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
            
            alert.view.tintColor = UIColor.black
            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.gray
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            present(alert, animated: true, completion: nil)
                   
                   let parameters = [
                     [
                       "key": "type",
                       "value": "add_song",
                       "type": "text"
                     ],
                     [
                       "key": "user_id",
                       "value": UserDefaults.standard.string(forKey: "userId")!,
                       "type": "text"
                     ],
                     [
                       "key": "title",
                       "value": "\(songDict!["songTitle"] ?? "")",
                       "type": "text"
                     ],
                     [
                       "key": "genre",
                       "value": "\(songDict!["songGenre"] ?? "")",
                       "type": "text"
                     ],
                     [
                       "key": "song_key",
                       "value": "\(songDict!["songStyle"] ?? "")",
                       "type": "text"
                     ],
                     [
                       "key": "bpm",
                       "value": "\(songDict!["songBpm"] ?? "")",
                       "type": "text"
                     ],
                     [
                       "key": "first",
                       "value": "\(songDict!["1st verse_lyrics"] ?? "")",
                       "type": "text"
                     ],
                     [
                       "key": "second",
                       "value": "\(songDict!["2nd verse_lyrics"] ?? "")",
                       "type": "text"
                     ],
                     [
                       "key": "bsection",
                       "value": "\(songDict!["b section_lyrics"] ?? "")",
                       "type": "text"
                     ],
                     [
                       "key": "hook",
                       "value": "\(songDict!["hook_lyrics"] ?? "")",
                       "type": "text"
                     ],
                     [
                       "key": "bridge",
                       "value": "\(songDict!["bridge_lyrics"] ?? "")",
                       "type": "text"
                     ],
                     [
                       "key": "song_path",
                       "value": "\(self.url?.absoluteString)",
                       "type": "text"
                     ]] as [[String : Any]]

                   let boundary = "Boundary-\(UUID().uuidString)"
                   var body = ""
                   var _: Error? = nil
                   for param in parameters {
                     if param["disabled"] == nil {
                       let paramName = param["key"]!
                       body += "--\(boundary)\r\n"
                       body += "Content-Disposition:form-data; name=\"\(paramName)\""
                       let paramType = param["type"] as! String
                       if paramType == "text" {
                         let paramValue = param["value"] as! String
                         body += "\r\n\r\n\(paramValue)\r\n"
                       } else {
                         let paramSrc = param["src"] as! String
                         let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                         let fileContent = String(data: fileData!, encoding: .utf8)!
                         body += "; filename=\"\(paramSrc)\"\r\n"
                           + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                       }
                     }
                   }
                   body += "--\(boundary)--\r\n";
                   let postData = body.data(using: .utf8)

                   var request = URLRequest(url: URL(string: "https://7tracking.com/api/api.php")!,timeoutInterval: Double.infinity)
                   request.addValue("recent_songs", forHTTPHeaderField: "type")
                   request.addValue("1", forHTTPHeaderField: "user_id")
                   request.addValue("PHPSESSID=n18qits4o0v908r7c4kusse4d2", forHTTPHeaderField: "Cookie")
                   request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

                   request.httpMethod = "POST"
                   request.httpBody = postData

                   let task = URLSession.shared.dataTask(with: request) { data, response, error in
                     guard let data = data else {
                       print(String(describing: error))
                       return
                     }
                     print(String(data: data, encoding: .utf8)!)
                    alert.dismiss(animated: true, completion: nil)
                       self.semaphore.signal()
                   }

                   task.resume()
                   semaphore.wait()

               }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

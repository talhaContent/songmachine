//
//  WriteVerseView.swift
//  SongMachine
//
//  Created by Technoventive on 03/12/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import Foundation
import UIKit


class WriteVerseView : UIView, UIKeyInput{
    var input = "Hello world"

        override var canBecomeFirstResponder: Bool {
            true
        }

        var hasText: Bool {
            input.isEmpty == false
        }

        func insertText(_ text: String) {
            input += text
            setNeedsDisplay()
        }

        func deleteBackward() {
            _ = input.popLast()
            setNeedsDisplay()
        }
    override init(frame: CGRect) {
        super.init(frame: frame)
//        var textfield = UITextField(frame: CGRect(x: self.frame.minX, y: self.frame.minY + 30, width: self.frame.size.width, height: self.frame.height - 30))
//        
//        self.addSubview(textfield)
//        textfield.bringSubviewToFront(self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
    override func draw(_ rect: CGRect) {
            let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 32)]
            let attributedString = NSAttributedString(string: input, attributes: attrs)
            attributedString.draw(in: rect)
            
        }
    
}

//
//  Verses_ViewController.swift
//  SongMachine
//
//  Created by SAM on 13/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit
import AVFoundation
import iOSDropDown
import MZFormSheetPresentationController

protocol RecordingDataDelegate {
    func updateAudioArray(updateSongDataDict:Dictionary<String,Any>)
    
}


class Verses_ViewController: UIViewController, RecordingDataDelegate, UITextFieldDelegate{
    
    
    enum CardState {
         case expanded
         case collapsed
     }
    @IBOutlet weak var mainScrollView: UIScrollView!
    var actionViewController: Record_ViewController!
    var visualEffectView:UIVisualEffectView!
    
    let cardHeight:CGFloat = 390
    let cardHandleAreaHeight:CGFloat = 65
    
    var cardVisible = false
    var nextState:CardState {
        return cardVisible ? .collapsed : .expanded
    }
    var runningAnimations = [UIViewPropertyAnimator]()
    var animationProgressWhenInterrupted:CGFloat = 0
    
    var bpm = ["97","140","130","120"]
    var selectedBpm : String?
    
    
    func updateAudioArray(updateSongDataDict: Dictionary<String, Any>) {
        self.dismiss(animated: true){
            self.songDataDict = updateSongDataDict
        }
    }
    var semaphore = DispatchSemaphore (value: 0)

//AllFields
    var player: AVAudioPlayer?

    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var verseone: BorderedTextView!
    
    @IBOutlet weak var versetwo: BorderedTextView!
    @IBOutlet weak var bsection: BorderedTextView!
    
    @IBOutlet weak var bpm_txtField: UITextField!{
        didSet {
            //bpm_txtField.tintColor = UIColor.lightGray
            bpm_txtField.setIcon(UIImage(imageLiteralResourceName: "bpm_arrow"))
        }
     }
    @IBOutlet weak var bridge: BorderedTextView!
    @IBOutlet weak var hook: BorderedTextView!

    var mainView_YAxis: CGFloat = 0.0
    
    
    
    @IBOutlet var detailtxt: UILabel!
    @IBOutlet var titleTxt: UILabel!
    var songDataDict: Dictionary<String, Any>?
    var audioFiles: Array<Any> = []
    var songDict: Array<Dictionary<String, Any>>?
//    func assignInputView(){
//        let navigationController = self.storyboard!.instantiateViewController(withIdentifier: "formSheetController") as! UINavigationController
//        let formSheetController = MZFormSheetPresentationViewController(contentViewController: self)
//        formSheetController.presentationController?.contentViewSize = CGSize(width: 250, height: 250)
//        verseone.inputView = formSheetController.view
//    }
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        //view.addGestureRecognizer(tap)
        //assignInputView()
        
        mainScrollView.isDirectionalLockEnabled = true
        //adjustUITextViewHeight(arg: hook)
        hideKeyboard()
        self.createPickerView()
        self.dismissPickerView()
        mainView_YAxis = mainView.frame.origin.y
        setFieldsData()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    func createPickerView() {
           let genrePickerview = UIPickerView()
        genrePickerview.delegate = self
        genrePickerview.dataSource = self
        genrePickerview.backgroundColor = UIColor.black
        //genrePickerview.
        genrePickerview.tag = 1
        
        bpm_txtField.inputView = genrePickerview
        
        
    }
    func dismissPickerView() {
       let toolBar = UIToolbar()
       toolBar.sizeToFit()
        toolBar.backgroundColor = UIColor(r: 35, g: 35, b: 52)
        toolBar.barTintColor = UIColor(r: 35, g: 35, b: 52)
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDone = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(self.dismissAction))
        let label = UILabel()
        label.text = "Select Genre"
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.clear
        let labelAsBarButtonItem = UIBarButtonItem(customView: label)
        
        let btnCancel = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(self.dismissAction))
        
        
        btnDone.tintColor = UIColor(r: 250, g: 112, b: 154)
         
        btnCancel.tintColor = UIColor(r: 250, g: 112, b: 154)
       toolBar.setItems([btnCancel,space,labelAsBarButtonItem,space,btnDone], animated: true)
       toolBar.isUserInteractionEnabled = true
       bpm_txtField.inputAccessoryView = toolBar
        
    }
    @objc func dismissAction() {
          view.endEditing(true)
    }
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            if(bsection.isFirstResponder == true){
                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight - (bsection.superview!.frame.origin.y + bsection.frame.size.height) - 150
            }
            else if(hook.isFirstResponder == true){
                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight - (hook.superview!.frame.origin.y + hook.frame.size.height)
            }
            else if(bridge.isFirstResponder == true){
                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight - (bridge.superview!.frame.origin.y + bridge.frame.size.height) - 150
            }
            else if(versetwo.isFirstResponder == true){
                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight - (versetwo.superview!.frame.origin.y + versetwo.frame.size.height) - 150
            }
        }
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            _ = keyboardRectangle.height
             if(bridge.isFirstResponder == true || bsection.isFirstResponder == true || hook.isFirstResponder == true){
            mainView.frame.origin.y = mainView_YAxis
            }
            mainView.frame.origin.y = mainView_YAxis
        }
    }
    func setBpmField(bpm:String){
//        bpm_txtField.optionArray = ["97","140","130","120"]
//        //Its Id Values and its optional
//        bpm_txtField.optionIds = [1,23,54,22]
//        bpm_txtField.checkMarkEnabled = false
        //self.bpm_txtField.inputView = UIView()
        self.bpm_txtField.text = bpm
        //self.bpm_txtField.delegate = self
//        bpm_txtField.didSelect {(selected, index, id) in
//            self.bpm_txtField.text = selected
//        }
       
    }
    func setFieldsData(){
        if(songDataDict != nil){
        let title = songDataDict!["songTitle"] as! String
        let genre = songDataDict!["songGenre"] as! String
        let style = songDataDict!["songStyle"] as! String
        let bpm   = songDataDict!["songBpm"] as! String
        var verseone_Lyrics = ""
        var versetwo_Lyrics = ""
        var bsection_Lyrics = ""
        var hook_Lyrics = ""
        var bridge_Lyrics = ""
        setBpmField(bpm:bpm)

        if(songDataDict!["1st verse_lyrics"] != nil){
            verseone_Lyrics = songDataDict!["1st verse_lyrics"] as! String
        }
        if(songDataDict!["2nd verse_lyrics"] != nil){
            versetwo_Lyrics = songDataDict!["2nd verse_lyrics"] as! String
        }
        if(songDataDict!["b section_lyrics"] != nil){
            bsection_Lyrics = songDataDict!["b section_lyrics"] as! String
        }
        if(songDataDict!["hook_lyrics"] != nil){
            hook_Lyrics = songDataDict!["hook_lyrics"] as! String
            
        }
        if(songDataDict!["bridge_lyrics"] != nil){
            bridge_Lyrics = songDataDict!["bridge_lyrics"] as! String
        }

        
        titleTxt.text = title.capitalized
        detailtxt.text = "\(genre.capitalized)  \u{2022} \(style.capitalized)"
        verseone.text = verseone_Lyrics
        versetwo.text = versetwo_Lyrics
        bsection.text = bsection_Lyrics
        hook.text = hook_Lyrics
        bridge.text = bridge_Lyrics
        }
    
    }
    @IBAction func closeTapped(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBar_ViewController") as? TabBar_ViewController
        //        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //        vc!.songDataDict = dict
        //        self.navigationController?.pushViewController(vc!, animated: true)
        self.present(vc!, animated: true, completion: nil)
    }
    @IBAction func saveBtnTapped(_ sender: Any) {

        if(self.verifyAllFields() == true){
            self.addLyricsInDict()
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SongPlayer_ViewController") as? SongPlayer_ViewController
            let titleTxt = songDataDict!["songTitle"] as! String

            vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            let song_path = vc!.mergeAudioFiles(audioFileUrls: filterAudioURLArray()! as NSArray, title: titleTxt)
            //vc!.audioFiles = filterAudioURLArray()!
            //setFieldsData()
            vc!.songDataDict = songDataDict
            if(songDict == nil){
                songDict = []
            }
            songDict?.append(songDataDict!)
            vc!.songDict = songDict
            vc?.isFromSaved = true
            callServiceToSaveSong(songPath: song_path, viewController : vc!)
            
//            self.navigationController?.pushViewController(vc!, animated: true)
//        }
        }
    }
    func movetoNextView(){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SongPlayer_ViewController") as? SongPlayer_ViewController
        let titleTxt = songDataDict!["songTitle"] as! String

        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        let song_path = vc!.mergeAudioFiles(audioFileUrls: filterAudioURLArray()! as NSArray, title: titleTxt)
        //vc!.audioFiles = filterAudioURLArray()!
        //setFieldsData()
        vc!.songDataDict = songDataDict
        if(songDict == nil){
            songDict = []
        }
        songDict?.append(songDataDict!)
        vc!.songDict = songDict
        vc?.isFromSaved = true
        self.present(vc!, animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    func callServiceToSaveSong(songPath: String , viewController : UIViewController){
        
        let title = songDataDict!["songTitle"] as! String
        songDataDict!["songPath"] = songPath
        let genre = songDataDict!["songGenre"] as! String
        let style = songDataDict!["songStyle"] as! String
        let bpm = songDataDict!["songBpm"] as! String
        let firstVerseLyrics = songDataDict!["1st verse_lyrics"] as! String
        let secondVerseLyrics = songDataDict!["2nd verse_lyrics"] as! String
        let bsection = songDataDict!["b section_lyrics"] as! String
        let hook = songDataDict!["hook_lyrics"] as! String
        let bridge = songDataDict!["bridge_lyrics"] as! String
        
        print(title)
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        let parameters = [
          [
            "key": "type",
            "value": "add_song",
            "type": "text"
          ],
          [
            "key": "user_id",
            "value": UserDefaults.standard.string(forKey: "userId")!,
            "type": "text"
          ],
          [
            "key": "title",
            "value": title,
            "type": "text"
          ],
          [
            "key": "genre",
            "value": genre,
            "type": "text"
          ],
          [
            "key": "song_key",
            "value": style,
            "type": "text"
          ],
          [
            "key": "bpm",
            "value": bpm,
            "type": "text"
          ],
          [
            "key": "first",
            "value": firstVerseLyrics,
            "type": "text"
          ],
          [
            "key": "second",
            "value": secondVerseLyrics,
            "type": "text"
          ],
          [
            "key": "bsection",
            "value": bsection,
            "type": "text"
          ],
          [
            "key": "hook",
            "value": hook,
            "type": "text"
          ],
          [
            "key": "bridge",
            "value": bridge,
            "type": "text"
          ],
          [
            "key": "song_path",
            "value": "\(songPath)",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var _: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://7tracking.com/api/api.php")!,timeoutInterval: Double.infinity)
        request.addValue("recent_songs", forHTTPHeaderField: "type")
        request.addValue("1", forHTTPHeaderField: "user_id")
        request.addValue("PHPSESSID=n18qits4o0v908r7c4kusse4d2", forHTTPHeaderField: "Cookie")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async {
                alert.dismiss(animated: true, completion: {
                    print("WOHOOOOOOO")
                    ///self.movetoNextView()
                    self.present(viewController, animated: true, completion: nil)
                    
                })
            }
            
            self.semaphore.signal()
        }

        task.resume()
        semaphore.wait()

    }
    func hideKeyboard(){
        let bar = UIToolbar()
        let reset = UIBarButtonItem(title: "Hide Keyboard", style: .plain, target: self, action: #selector(hideTapped))
        let next = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextTextField))
        
        bar.items = [reset, next]
        bar.sizeToFit()
        verseone.inputAccessoryView = bar
        versetwo.inputAccessoryView = bar
        bsection.inputAccessoryView = bar
        hook.inputAccessoryView = bar
        bridge.inputAccessoryView = bar
        
    }
    @objc func nextTextField(sender: UIBarButtonItem){
        if(verseone.isFirstResponder == true){
            versetwo.becomeFirstResponder()
        }
        else if(versetwo.isFirstResponder == true){
            bsection.becomeFirstResponder()
        }
        else if(bsection.isFirstResponder == true){
            bridge.becomeFirstResponder()
            
        }
        else if(bridge.isFirstResponder == true){
            hook.becomeFirstResponder()
            
        }

    }
    @objc func hideTapped(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    func verifyAllFields() -> Bool{
        
        if((verseone.text?.elementsEqual(""))! || songDataDict!["1st verse"] == nil){
            showAlert(title: "Missing", msg: "Please add lyrics or record Verse one of the song.")
            return false
        }
        if((versetwo.text?.elementsEqual(""))! || songDataDict!["2nd verse"] == nil){
            showAlert(title: "Missing", msg: "Please add lyrics or record Verse Two of the song.")
            return false
        }
        if((bsection.text?.elementsEqual(""))! || songDataDict!["b section"] == nil){
            showAlert(title: "Missing", msg: "Please add lyrics or record B section of the song.")
            return false
        }
        if((hook.text?.elementsEqual(""))! || songDataDict!["hook"] == nil){
            showAlert(title: "Missing", msg: "Please add lyrics or record Hook of the song.")
            return false
        }
        if((bridge.text?.elementsEqual(""))! || songDataDict!["bridge"] == nil){
            showAlert(title: "Missing", msg: "Please add lyrics or record Bridge of the song.")
            return false
        }
        return true
    }
    func showAlert(title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("%@", songDataDict as Any)
        print(UserDefaults.standard.dictionaryRepresentation().keys)
    }
    func filterAudioURLArray() -> Array<Any>?{
        var arrTemp: Array<Any> = []
        var verse_one = URL(fileURLWithPath: "")
        var verse_two = URL(fileURLWithPath: "")
        var bSection = URL(fileURLWithPath: "")
        var hook = URL(fileURLWithPath: "")
        var bridge = URL(fileURLWithPath: "")
        
        for(key, value) in songDataDict!{
            if(key.elementsEqual("1st verse")){
                verse_one = value as! URL
            }
            else  if(key.elementsEqual("2nd verse")){
                verse_two = value as! URL
            }
            else  if(key.elementsEqual("b section")){
                bSection = value as! URL
            }
            else  if(key.elementsEqual("hook")){
                hook = value as! URL
            }
            else  if(key.elementsEqual("bridge")){
                bridge = value as! URL
            }
        }
        
        arrTemp.append(verse_one)
        arrTemp.append(bSection)
        arrTemp.append(hook)
        arrTemp.append(verse_two)
        arrTemp.append(bSection)
        arrTemp.append(hook)
        arrTemp.append(bridge)
        arrTemp.append(hook)

        return arrTemp
    }
    
    @IBAction func muteBtnTapped(_ sender: UIButton) {
        player?.stop()
    }
    @IBAction func playBtnTapped(_ sender: UIButton) {
        let tag = sender.tag
        if(songDataDict!.count > 0){
        if(tag == 1){
            if let url = songDataDict?["1st verse"]{
            playSound(url: url as! URL)
            }
        }
        else if(tag == 2){
            if let url = songDataDict?["2nd verse"]{
            playSound(url: url  as! URL)
            }
        }
        else if(tag == 3){
            if let url = songDataDict?["b section"]{
            playSound(url: url  as! URL)
            }
        }
        else if(tag == 4){
            if let url = songDataDict?["hook"]{
            playSound(url: url  as! URL)
            }
        }
        else if(tag == 5){
                if let url = songDataDict?["bridge"]{
                playSound(url: url  as! URL)
                }
            }
        }
    }
    func playSound(url: URL) {
        // let url = NSBundle.mainBundle().URLForResource("soundName", withExtension: "mp3")!
        
        do {
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            let session = AVAudioSession.sharedInstance()
            try session.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
            player.prepareToPlay()
            
            player.play()
            
        } catch let error as NSError {
            print(error.description)
        }
    }
    func addLyricsInDict(){
        if(songDataDict != nil){
            for i in 0..<5{
                if(i == 0){
                    songDataDict!["1st verse_lyrics"] = verseone.text
                }
                if(i == 1){
                    songDataDict!["2nd verse_lyrics"] = versetwo.text
                }
                if(i == 2){
                    songDataDict!["b section_lyrics"] = bsection.text
                }
                if(i == 3){
                    songDataDict!["hook_lyrics"] = hook.text
                }
                if(i == 4){
                    songDataDict!["bridge_lyrics"] = bridge.text
                }
            }
        }
    }
}
extension Verses_ViewController{
    
    
    @IBAction func recordBtnTapped(_ sender: UIButton) {
        //self.addLyricsInDict()
        setupCard(tag:sender.tag)
        animateTransitionIfNeeded(state: nextState, duration: 0.9)
    }
    func animateTransitionIfNeeded (state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    self.actionViewController.view.frame.origin.y = self.view.frame.height - self.cardHeight
                case .collapsed:
                    self.actionViewController.view.frame.origin.y = self.view.frame.height - self.cardHandleAreaHeight
                }
            }
            
            frameAnimator.addCompletion { _ in
                self.cardVisible = !self.cardVisible
                self.runningAnimations.removeAll()
            }
            
            frameAnimator.startAnimation()
            runningAnimations.append(frameAnimator)
            
            
            let cornerRadiusAnimator = UIViewPropertyAnimator(duration: duration, curve: .linear) {
                switch state {
                case .expanded:
                    self.actionViewController.view.layer.cornerRadius = 12
                case .collapsed:
                    self.actionViewController.view.layer.cornerRadius = 0
                }
            }
            
            cornerRadiusAnimator.startAnimation()
            runningAnimations.append(cornerRadiusAnimator)
            
            let blurAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    self.visualEffectView.effect = UIBlurEffect(style: .dark)
                case .collapsed:
                    self.visualEffectView.effect = nil
                }
            }
            
            blurAnimator.startAnimation()
            runningAnimations.append(blurAnimator)
            
        }
    }
    
    func startInteractiveTransition(state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            animateTransitionIfNeeded(state: state, duration: duration)
        }
        for animator in runningAnimations {
            animator.pauseAnimation()
            animationProgressWhenInterrupted = animator.fractionComplete
        }
    }
    
    func updateInteractiveTransition(fractionCompleted:CGFloat) {
        for animator in runningAnimations {
            animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
        }
    }
    
    func continueInteractiveTransition (){
        for animator in runningAnimations {
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }
    func setupCard(tag:Int) {
        visualEffectView = UIVisualEffectView()
        visualEffectView.frame = self.view.frame
        self.view.addSubview(visualEffectView)
        
        actionViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Record_ViewController") as? Record_ViewController
   
        actionViewController!.btnTag = tag
        actionViewController!.songDataDict = songDataDict
        actionViewController.RecordClassDelegate = self
        actionViewController.view.frame = CGRect(x: 0, y: self.view.frame.height - cardHandleAreaHeight, width: self.view.bounds.width, height: cardHeight)
        
        actionViewController.view.clipsToBounds = true
        
        self.view.addSubview(actionViewController.view)
        
      

        
    }
    
}
extension Verses_ViewController : UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
                scrollView.contentOffset.x = 0
            }
    }
    
}

extension Verses_ViewController : Protocol_Record_ViewController{
    func screenClosed() {
        self.visualEffectView.removeFromSuperview()
        self.cardVisible = !self.cardVisible
    }
    func closeAndSave(updateSongDataDict: Dictionary<String, Any>) {
        self.visualEffectView.removeFromSuperview()
        self.cardVisible = !self.cardVisible
        self.songDataDict = updateSongDataDict
    }
    
    
}


extension Verses_ViewController :  UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bpm.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let cell = pickerView.view(forRow: row, forComponent: component)
        cell?.tintColor = UIColor.red
        selectedBpm = bpm[row]
        bpm_txtField.text = selectedBpm//UIColor(r: 73, g: 73, b: 90)
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: bpm[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
            return attributedString
    }
    
}

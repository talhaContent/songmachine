//
//  Record_ViewController.swift
//  SongMachine
//
//  Created by SAM on 15/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

protocol Protocol_Record_ViewController : class {
    func screenClosed ()
    func closeAndSave(updateSongDataDict:Dictionary<String,Any>)
}

class Record_ViewController: UIViewController, AVAudioRecorderDelegate, UIGestureRecognizerDelegate{

    enum CardState {
           case expanded
           case collapsed
       }
      var actionViewController: Play_Verses_ViewController!
    @IBOutlet weak var toolbar: UIToolbar!
    var visualEffectView:UIVisualEffectView!
    var delegate:RecordingDataDelegate?
      let cardHeight:CGFloat = 388
      let cardHandleAreaHeight:CGFloat = 65
    weak var RecordClassDelegate : Protocol_Record_ViewController?
      var cardVisible = false
      var nextState:CardState {
          return cardVisible ? .collapsed : .expanded
      }
        let label = UILabel()
      var runningAnimations = [UIViewPropertyAnimator]()
      var animationProgressWhenInterrupted:CGFloat = 0
    var totalTime = 0
    var constTime = 0
    
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var recordingLabel: UILabel!
    @IBOutlet var timerLAbel: UILabel!
    @IBOutlet var recordBtn_one: UIButton!
    
    @IBOutlet var circularProgressView: CircularProgressView!
    
    
    
    var fileLocationDict: Dictionary<String, Any>?
    var songDataDict: Dictionary<String, Any>?

    var urlArr:Array<Any>!
    
    var btnTag: Int = 0
    var timer: Timer!

    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder?
    var verseTxt:String = ""
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    func createToolbar(){
       
        toolbar.sizeToFit()
        toolbar.backgroundColor = UIColor(r: 35, g: 35, b: 52)
        toolbar.barTintColor = UIColor(r: 35, g: 35, b: 52)
         let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
         let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(DoneTapped))
         label.textColor = UIColor.white
         label.backgroundColor = UIColor.clear
         let labelAsBarButtonItem = UIBarButtonItem(customView: label)
         
         let btnCancel = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(closeTapped))
         
         
         btnDone.tintColor = UIColor(r: 250, g: 112, b: 154)
          
         btnCancel.tintColor = UIColor(r: 250, g: 112, b: 154)
        toolbar.setItems([btnCancel,space,labelAsBarButtonItem,space,btnDone], animated: true)
        toolbar.isUserInteractionEnabled = true
        
        
        
    }
    func setToolbarTitle(lbl : String){
        
    }
    @objc func closeTapped(){
        RecordClassDelegate?.screenClosed()
        self.view.removeFromSuperview()
    }
    @objc func DoneTapped(){
        setupCard()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createToolbar()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
        
        recordingSession = AVAudioSession.sharedInstance()

        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        //addLongPressGesture()
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(HoldDownGesture(sender:)))
        recordBtn_one.addGestureRecognizer(longGesture)
        setTitle()
        recordingLabel.text = "Press & Hold to record"
        //recordBtn_one.addTarget(self, action: #selector(self.holdRelease(sender:)), for: UIControl.Event.touchUpInside);
        //recordBtn_one.addTarget(self, action: #selector(self.HoldDown(sender:)), for: UIControl.Event.touchDown)

    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    func setTitle(){
        
        if(btnTag == 0){
            label.text = "Record 1st Verse"
            
            verseTxt = "1st Verse".lowercased()
        }
        if(btnTag == 1){
            label.text = "Record 2nd Verse"
            
            verseTxt = "2nd Verse".lowercased()
        }
        if(btnTag == 2){
            label.text = "Record B Section"
            
            verseTxt = "B Section".lowercased()
        }
        if(btnTag == 3){
            label.text = "Record Hook"
            
            verseTxt = "Hook".lowercased()
        }
        if(btnTag == 4){
            label.text = "Record Bridge"
            verseTxt = "Bridge".lowercased()
        }
    }
    func Btntapped(tag:Int){
        
    }
    func loadRecordingUI() {
//        recordBtn_one = UIButton(frame: CGRect(x: 64, y: 64, width: 128, height: 64))
//        recordButton.setTitle("Tap to Record", for: .normal)
//        recordButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .title1)
//        recordBtn_one.addTarget(self, action: #selector(recordTapped), for: .touchUpInside)
//        view.addSubview(recordButton)
    }
    func startRecording() {
        fileLocationDict = Dictionary()
        urlArr = Array()
        let key = "\(verseTxt.trimmingCharacters(in: CharacterSet.whitespaces)).m4a"
        let audioFilename = getDocumentsDirectory().appendingPathComponent(key)
        urlArr.append(audioFilename)
        fileLocationDict?[key] = audioFilename
        print(audioFilename)
        print("%@",fileLocationDict as Any)
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder?.delegate = self
            audioRecorder?.record()

           // recordBtn_one.setTitle("Tap to Stop", for: .normal)
        } catch {
            finishRecording(success: false)
        }
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func finishRecording(success: Bool) {
        audioRecorder?.stop()
        audioRecorder = nil
        
        if success {
            //recordBtn_one.setTitle("Tap to Re-record", for: .normal)
        } else {
            //recordBtn_one.setTitle("Tap to Record", for: .normal)
            // recording failed :(
        }
    }
    
    func recordAudioFile(){
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.startRecording()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        
    }
    //target functions
    
    
    @objc func HoldDown(sender:UIButton)
        {
        
            recordAudioFile()
            var constTime = 0
            var totalTime = 0
            
            if(btnTag == 0){ // 1st Verse
                totalTime = 20
            }
            else if(btnTag == 1){ // 2st Verse
                totalTime = 20
            }
            else if(btnTag == 2){ // B Section
                totalTime = 10
            }
            else if(btnTag == 3){ // Hook
                totalTime = 20
            }
            else if(btnTag == 4){ // Bridge
                totalTime = 10
            }
            self.circularProgressView.progressAnimation(duration: TimeInterval(totalTime))
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                let hours = Int(constTime) / 3600
                let minutes = Int(constTime) / 60 % 60
                let seconds = Int(constTime) % 60
                let timeText = String(format:"%02i:%02i:%02i", hours, minutes, seconds)
                constTime = constTime + 1
                self.timerLAbel.text = timeText
                self.recordingLabel.text = "Recording"
          
//                 let randomNumber = Int.random(in: 1...20)
//                 print("Number: \(randomNumber)")

                 if constTime == totalTime {
                    constTime = 0
                    timer.invalidate()
                    self.goToNextViewController()
                 }
             }
        }

     @objc func HoldDownGesture(sender:UILongPressGestureRecognizer)
    {
        
        if(sender.state == UIGestureRecognizer.State.began){
            constTime = 0
            recordAudioFile()
            
            if(btnTag == 0){ // 1st Verse
                totalTime = 20
            }
            else if(btnTag == 1){ // 2st Verse
                totalTime = 20
            }
            else if(btnTag == 2){ // B Section
                totalTime = 10
            }
            else if(btnTag == 3){ // Hook
                totalTime = 20
            }
            else if(btnTag == 4){ // Bridge
                totalTime = 10
            }
            self.circularProgressView.progressAnimation(duration: TimeInterval(totalTime))
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [self] timer in
                let hours = Int(self.constTime) / 3600
                let minutes = Int(self.constTime) / 60 % 60
                let seconds = Int(self.constTime) % 60
                let timeText = String(format:"%02i:%02i:%02i", hours, minutes, seconds)
                self.constTime = constTime + 1
                self.timerLAbel.text = timeText
                self.recordingLabel.text = "Recording"
                if self.constTime == totalTime {
                    self.constTime = 0
                        timer.invalidate()
                        self.goToNextViewController()
                    }
                }
        }else if(sender.state == UIGestureRecognizer.State.ended){
            
            timer.invalidate()
            timer = nil
            //self.circularProgressView.
            self.circularProgressView.progreeTo(val: Double(constTime)/Double(totalTime))
            self.goToNextViewController()
        }
        
                
                
    }
 @objc func holdRelease(sender:UIButton)
        {
            print("Stop") // Start recording the video
            self.goToNextViewController()
        }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    @objc func longPress(gesture: UILongPressGestureRecognizer) {
        if gesture.state == UIGestureRecognizer.State.began {
            print("Long Press")
        }
    }
    func addLongPressGesture(){
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
        longPress.minimumPressDuration = 1.5
        self.recordBtn_one.addGestureRecognizer(longPress)
    }

}
extension Record_ViewController{
    
    func goToNextViewController(){
        //timer.invalidate()
        finishRecording(success: true)
        
    }
    
    
    func animateTransitionIfNeeded (state:CardState, duration:TimeInterval) {
         if runningAnimations.isEmpty {
             let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                 switch state {
                 case .expanded:
                     self.actionViewController.view.frame.origin.y = self.view.frame.height - self.cardHeight
                 case .collapsed:
                     self.actionViewController.view.frame.origin.y = self.view.frame.height - self.cardHandleAreaHeight
                 }
             }
             
             frameAnimator.addCompletion { _ in
                 self.cardVisible = !self.cardVisible
                 self.runningAnimations.removeAll()
             }
             
             frameAnimator.startAnimation()
             runningAnimations.append(frameAnimator)
             
             
             let cornerRadiusAnimator = UIViewPropertyAnimator(duration: duration, curve: .linear) {
                 switch state {
                 case .expanded:
                     self.actionViewController.view.layer.cornerRadius = 12
                 case .collapsed:
                     self.actionViewController.view.layer.cornerRadius = 0
                 }
             }
             
             cornerRadiusAnimator.startAnimation()
             runningAnimations.append(cornerRadiusAnimator)
             
             let blurAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                 switch state {
                 case .expanded:
                     self.visualEffectView.effect = UIBlurEffect(style: .dark)
                 case .collapsed:
                     self.visualEffectView.effect = nil
                 }
             }
             
             blurAnimator.startAnimation()
             runningAnimations.append(blurAnimator)
             
         }
     }
     
     func startInteractiveTransition(state:CardState, duration:TimeInterval) {
         if runningAnimations.isEmpty {
             animateTransitionIfNeeded(state: state, duration: duration)
         }
         for animator in runningAnimations {
             animator.pauseAnimation()
             animationProgressWhenInterrupted = animator.fractionComplete
         }
     }
     
     func updateInteractiveTransition(fractionCompleted:CGFloat) {
         for animator in runningAnimations {
             animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
         }
     }
     
     func continueInteractiveTransition (){
         for animator in runningAnimations {
             animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
         }
     }
    func setupCard() {
        print(self.view.subviews)
         visualEffectView = UIVisualEffectView()
         visualEffectView.frame = self.view.frame
         actionViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Play_Verses_ViewController") as? Play_Verses_ViewController
        actionViewController!.url = fileLocationDict!["\(verseTxt).m4a"] as? URL
        actionViewController!.songDataDict = songDataDict
        actionViewController!.urlDict = fileLocationDict
        actionViewController!.verseTitle = verseTxt
        actionViewController!.audioArr = urlArr
        actionViewController.addRecodingDelegate = self
        actionViewController!.screenTitle = label.text ?? ""
        actionViewController.view.frame = CGRect(x: 0, y: self.view.superview?.frame.height ?? self.view.frame.height - cardHandleAreaHeight, width: self.view.bounds.width, height: cardHeight)
        actionViewController.view.clipsToBounds = true
        self.view.addSubview(actionViewController.view)
        animateTransitionIfNeeded(state: nextState, duration: 0.9)
     }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
}
extension Record_ViewController : Play_Verses_Protocol{
    func addRecording(songDataDict: Dictionary<String, Any>) {
        self.songDataDict = songDataDict
        RecordClassDelegate?.closeAndSave(updateSongDataDict: self.songDataDict!)
        self.view.removeFromSuperview()
    }
    func goBack(){
        self.visualEffectView.removeFromSuperview()
        actionViewController.view.removeFromSuperview()
        self.cardVisible = !self.cardVisible
        setTitle()
        recordingLabel.text = "Press & Hold to record"
        timerLAbel.text = ""
        //self.circularProgressView.progressAnimation(duration: TimeInterval(20))
        self.circularProgressView.progreeTo(val: 0)
    }
}

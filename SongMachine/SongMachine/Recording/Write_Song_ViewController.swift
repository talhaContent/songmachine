//
//  Write_Song_ViewController.swift
//  SongMachine
//
//  Created by SAM on 13/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit
import iOSDropDown

protocol  wrtieSongDelegate {
    func closeTapped()
}

class Write_Song_ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDataSource {

    var dict: Dictionary<String, Any>?
    
    @IBOutlet weak var titleTxt: BorderedTextField!
    var delegate : wrtieSongDelegate?
    
    
    @IBOutlet weak var styleTxt: BorderedDropdownTextField!
    //@IBOutlet weak var genreTxt: TextField!
    
    @IBOutlet weak var genreTxt: BorderedDropdownTextField!
    @IBOutlet weak var bpmTxt: BorderedDropdownTextField!
    
    
    
    
    var selectedGenre: String?
    var selectedStyle : String?
    var selectedBpm : String?
    
    var bpm = ["97","140","130","120"]
    var style = ["Male","Female","Duet"]
    var genre = ["Country","Hip-Hop","Rock"]
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapp = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
//
        self.view.addGestureRecognizer(tapp)
        self.createPickerView()
        self.dismissPickerView()
        self.genreTxt.delegate = self
        self.styleTxt.delegate = self
        self.bpmTxt.delegate = self
        self.disableTextviews()
        
        hideKeyboard()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )

    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            //            if(nameTxtField.isFirstResponder == true){
            //                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight - nameTxtField.frame.origin.y
            //            }
            //            else if(emailTxtField.isFirstResponder == true){
            //                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight - emailTxtField.frame.origin.y
            //            }
//            if(passwordTxtField.isFirstResponder == true){
//                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight - (passwordTxtField.frame.origin.y + passwordTxtField.frame.size.height)
//            }
//            else if(confirmPasswordTxtField.isFirstResponder == true){
//                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight - (confirmPasswordTxtField.frame.origin.y + confirmPasswordTxtField.frame.size.height)
//            }
        }
        //        scrollView.frame = CGRect(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height-220);
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
//            mainView.frame.origin.y = mainView_YAxis
            //            if(nameTxtField.isFirstResponder == true){
            //                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight + nameTxtField.frame.size.height
            //            }
            //            else if(emailTxtField.isFirstResponder == true){
            //                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight + emailTxtField.frame.size.height
            //            }
            //             if(passwordTxtField.isFirstResponder == true){
            //                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight + (passwordTxtField.frame.size.height)
            //            }
            //            else if(confirmPasswordTxtField.isFirstResponder == true){
            //                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight + (confirmPasswordTxtField.frame.size.height)
            //            }
        }
        //        scrollView.frame = CGRect(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height+220);
    }
    func hideKeyboard(){
        let bar = UIToolbar()
        let reset = UIBarButtonItem(title: "Hide Keyboard", style: .plain, target: self, action: #selector(hideTapped))
        bar.items = [reset]
        bar.sizeToFit()
        titleTxt.inputAccessoryView = bar
        
        
    }
    
    
    
    @objc func genreTapped()  {
        //self.setGenreDropDownData()
        //genreTxt.showList()
        self.view.endEditing(true)

        
    }

//    func setGenreDropDownData(){
//        genreTxt.optionArray = ["Hip-Hop", "Dance"]
//        //Its Id Values and its optional
////        dropDown.optionIds = [1,23,54,22]
//
//        // Image Array its optional
////        genreTxt.ImageArray = [👩🏻‍🦳,🙊]
//        // The the Closure returns Selected Index and String
//        genreTxt.didSelect{(selectedText , index ,id) in
//        self.view.endEditing(true)
//
//        self.genreTxt.text = selectedText
//    }
//    }
    
    
    @IBAction func songKeyTapped(_ sender: Any) {
        self.view.endEditing(true)
    }
    @IBAction func bpmTapped(_ sender: Any) {
        self.view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func getDataInDict() -> Dictionary<String, Any>{
        var tempDict = Dictionary<String, Any>()
        tempDict["songTitle"] = titleTxt.text!
        tempDict["songGenre"] = genreTxt.text!
        tempDict["songStyle"] = styleTxt.text!
        tempDict["songBpm"] = bpmTxt.text!

        return tempDict
    }
    @IBAction func continueBtnTapped(_ sender: Any) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let pvc = storyboard.instantiateViewController(withIdentifier: "Verses_ViewController")
        if(self.verifyAllFields() == true){
        dict = getDataInDict()
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Verses_ViewController") as? Verses_ViewController
        //        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc!.songDataDict = dict
//        self.navigationController?.pushViewController(vc!, animated: true)
        self.present(vc!, animated: true, completion: nil)
        }
    }
    
    func disableTextviews(){
        
//        self.genreTxt.isUserInteractionEnabled = false
//        self.styleTxt.isUserInteractionEnabled = false
//        self.bpmTxt.isUserInteractionEnabled = false
    }
    @objc func hideTapped(sender: UIBarButtonItem){
           self.view.endEditing(true)
       }
       
    
    @IBAction func closeTapped(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBar_ViewController") as? TabBar_ViewController
            //        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
    //        vc!.songDataDict = dict
            //        self.navigationController?.pushViewController(vc!, animated: true)
        self.present(vc!, animated: true, completion: nil)
        //delegate?.closeTapped()
        //self.view.removeFromSuperview()
//        let tabbar = TabBar_ViewController()
//        tabbar.selectedIndex = 0
//        self.move(tabbar, animated: true, completion: nil)
        
    }
    
    func verifyAllFields() -> Bool{
        if((titleTxt.text?.elementsEqual(""))!){
            showAlert(title: "Missing", msg: "Please enter title of the song.")
            return false
        }
        if((genreTxt.text?.elementsEqual(""))!){
            showAlert(title: "Missing", msg: "Please enter Genre of the song.")
            return false
        }
        if((styleTxt.text?.elementsEqual(""))!){
            showAlert(title: "Missing", msg: "Please enter song key of the song.")
            return false
        }
        if((bpmTxt.text?.elementsEqual(""))!){
            showAlert(title: "Missing", msg: "Please enter bpm of the song.")
            return false
        }
        return true
    }
    func showAlert(title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func createPickerView() {
           let genrePickerview = UIPickerView()
        genrePickerview.delegate = self
        genrePickerview.dataSource = self
        genrePickerview.backgroundColor = UIColor.black
        //genrePickerview.
        genrePickerview.tag = 1
        
        genreTxt.inputView = genrePickerview
        
        let songKeyPickerView = UIPickerView()
        songKeyPickerView.delegate = self
        songKeyPickerView.dataSource = self
        songKeyPickerView.backgroundColor = UIColor.black
        songKeyPickerView.tag = 2
        styleTxt.inputView = songKeyPickerView
        
        let bpmPickerView = UIPickerView()
        bpmPickerView.delegate = self
        bpmPickerView.dataSource = self
        bpmPickerView.backgroundColor = UIColor.black
        bpmPickerView.tag = 3
        bpmTxt.inputView = bpmPickerView
        
        
        
    }
    func dismissPickerView() {
       let toolBar = UIToolbar()
       toolBar.sizeToFit()
        toolBar.backgroundColor = UIColor(r: 35, g: 35, b: 52)
        toolBar.barTintColor = UIColor(r: 35, g: 35, b: 52)
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDone = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(self.dismissAction))
        let label = UILabel()
        label.text = "Select Genre"
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.clear
        let labelAsBarButtonItem = UIBarButtonItem(customView: label)
        
        let btnCancel = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(self.dismissAction))
        
        
        btnDone.tintColor = UIColor(r: 250, g: 112, b: 154)
         
        btnCancel.tintColor = UIColor(r: 250, g: 112, b: 154)
       toolBar.setItems([btnCancel,space,labelAsBarButtonItem,space,btnDone], animated: true)
       toolBar.isUserInteractionEnabled = true
       genreTxt.inputAccessoryView = toolBar
        styleTxt.inputAccessoryView = toolBar
        bpmTxt.inputAccessoryView = toolBar
    }
    @objc func dismissAction() {
          view.endEditing(true)
    }
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        return false
//    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        return false
//    }
    
}

extension Write_Song_ViewController:UIPopoverPresentationControllerDelegate
{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}


extension Write_Song_ViewController : UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1 // number of session
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 1){
            return genre.count
        }else if(pickerView.tag == 2){
            return style.count
        }else if(pickerView.tag == 3){
            return bpm.count
        }
         return 1// number of dropdown items
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        if(pickerView.tag == 1){
            let attributedString = NSAttributedString(string: genre[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
                return attributedString
            
        }else if(pickerView.tag == 2){
            let attributedString = NSAttributedString(string: style[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
                return attributedString
            
        }else if(pickerView.tag == 3){
            let attributedString = NSAttributedString(string: bpm[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
            
                return attributedString
            
        }
        return NSAttributedString()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let cell = pickerView.view(forRow: row, forComponent: component)
        cell?.tintColor = UIColor.red//UIColor(r: 73, g: 73, b: 90)
        if(pickerView.tag == 1){
            selectedGenre = genre[row] // selected item
            genreTxt.text = selectedGenre
        }else if(pickerView.tag == 2){
            selectedStyle = style[row]
            styleTxt.text = selectedStyle
        }else if(pickerView.tag == 3){
            selectedBpm = bpm[row]
            bpmTxt.text = selectedBpm
        }
        
    }

}

class TextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

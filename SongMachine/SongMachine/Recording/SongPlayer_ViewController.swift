//
//  SongPlayer_ViewController.swift
//  SongMachine
//
//  Created by SAM on 22/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit
import AVFoundation

class SongPlayer_ViewController: UIViewController {

    var mergeAudioURL = NSURL()
    var mergerAudioURLArray = NSArray()
    var audioFiles: Array<Any> = []
    var songDataDict: Dictionary<String, Any>?
    var titleTxt: String = ""
    var genreTxt: String = ""
    @IBOutlet weak var btnPrevious: UIButton!
    var audioPlayer : AVAudioPlayer?
    @IBOutlet weak var btnNext: UIButton!
    var toggleState = 1 {
        didSet{
            if toggleState == 1{
                playBtn.setImage(UIImage(named:"Play-1.png"),for:.normal)
                
            }else{
                playBtn.setImage(UIImage(named:"pause.png"),for:.normal)
            }
        }
    }
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet var slider: UISlider!
    @IBOutlet var playedTime: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var lyricsTxt: UITextView!
    var currentTime : TimeInterval?
    var songDict: Array<Dictionary<String, Any>>?
    var songArray : NSArray?
    var model: [SongModel]?
    var currentSong : Int?
    var x = 1
    var headphonesConnected : Bool?
    @IBAction func done(sender: AnyObject) {
        dismiss(animated: true, completion: nil)
        audioPlayer?.stop()
    }
    var isFromSaved : Bool?
//    @IBAction func play(sender: AnyObject) {
//        audioPlayer.play()
//        updateTime()
//    }
//    
//    @IBAction func pause(sender: AnyObject) {
//        audioPlayer.pause()
//        updateTime()
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getCurrentIndex()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.audioPlayer = AVAudioPlayer()
        self.view.addGestureRecognizer(tap)
        setupNotifications()
        self.playBtn.layer.cornerRadius = 0.5 * playBtn.bounds.size.width
        if(songDataDict != nil && songDataDict!.count > 0){
        titleTxt = songDataDict!["songTitle"] as! String
        }
        if(isFromSaved ?? false){
            btnPrevious.isHidden = true
            btnNext.isHidden = true
        }else{
            btnPrevious.isHidden = false
            btnNext.isHidden = false
        }
        
        titleLabel.text = titleTxt
        updateSongLyrics()
        
        _ = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
    
        
        let pathURL = mergeAudioURL//mergeAudioFiles(audioFileUrls: mergerAudioURLArray, title: songDataDict!["songTitle"] as! String)
            
            do {
                
                try audioPlayer = AVAudioPlayer(contentsOf: pathURL as URL)
                
            } catch {
                
                print("error")
            }
        slider.maximumValue = Float(audioPlayer?.duration ?? 0)
    }
    func reloadView(){
        
        if let currentSong = self.currentSong{
            songDataDict?["songTitle"] = model?[currentSong].title!
            songDataDict?["songGenre"] = model?[currentSong].genre!
            songDataDict?["songPath"] = model?[currentSong].song_path
            songDataDict?["songStyle"] = model?[currentSong].song_key
            songDataDict?["songBpm"] = model?[currentSong].bpm!
            songDataDict?["1st verse_lyrics"] = model?[currentSong].first!
            songDataDict?["2nd verse_lyrics"] = model?[currentSong].second!
            songDataDict?["b section_lyrics"] = model?[currentSong].bsection!
            songDataDict?["hook_lyrics"] = model?[currentSong].hook!
            songDataDict?["bridge_lyrics"] = model?[currentSong].bridge!
        }
        
        if(songDataDict != nil && songDataDict!.count > 0){
        titleTxt = songDataDict!["songTitle"] as! String
        }
        let fileUrl = NSURL(fileURLWithPath: songDataDict?["songPath"] as! String)
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        _ = documentsPath as String + "/" + String(fileUrl.absoluteURL?.lastPathComponent ?? "")
        let url = NSURL(fileURLWithPath: documentsPath as String + "/" + String(fileUrl.absoluteURL?.lastPathComponent ?? ""))
        mergeAudioURL = url
        titleLabel.text = titleTxt
        updateSongLyrics()
        
        _ = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        _ = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
        
        
        let pathURL = mergeAudioURL//mergeAudioFiles(audioFileUrls: mergerAudioURLArray, title: songDataDict!["songTitle"] as! String)
            
            do {
                
                try audioPlayer = AVAudioPlayer(contentsOf: pathURL as URL)
                
            } catch {
                
                print("error")
            }
        slider.maximumValue = Float(audioPlayer?.duration ?? 0)
        self.view.setNeedsLayout()
    }
    func getCurrentIndex(){
        if let m = model{
            var i = 0
            for s in m{
                if(s.song_path == songDataDict?["songPath"] as? String){
                   currentSong = i
                    break
                }
                i+=1
            }
        }
    }
    
    
    @IBAction func fastRewindTapped(_ sender: Any) {
        
        let tme = audioPlayer?.currentTime;
        let t = tme?.advanced(by: -2.0)
        if (t ?? TimeInterval()  > audioPlayer?.duration ?? 0.0)
        {
            // stop, track skip or whatever you want
        }
        else{
            audioPlayer?.currentTime = t ?? TimeInterval();
        }
    }
    @IBAction func fastForwardTapped(_ sender: Any) {
        let tme = audioPlayer?.currentTime;
        let t = tme?.advanced(by: 2.0)
        if (t ?? TimeInterval()  > audioPlayer?.duration ?? 0.0)
        {
            // stop, track skip or whatever you want
        }
        else{
            audioPlayer?.currentTime = t ?? TimeInterval();
        }
    }
    @IBAction func nextBtnTapped(_ sender: Any) {
        if let c = self.currentSong{
            if(c < self.model!.count - 1){
                currentTime = 0.0
                toggleState = 1
                currentSong!+=1
                reloadView()
                
            }else{
                print("sadadasdas")
            }
        }
        
        
        
        self.view.setNeedsLayout()
    }
    @IBAction func previousBtnTapped(_ sender: Any) {
        if let c = self.currentSong{
            if(c > 0){
                currentTime = 0.0
                toggleState = 1
                currentSong!-=1
                reloadView()
                
            }else{
                print("sadadasdas")
            }
        }
    }
    @IBAction func stop(sender: AnyObject) {
        audioPlayer?.stop()
        updateTime()
    }
    
    @IBAction func scrubAudio(sender: AnyObject) {
        audioPlayer?.stop()
        audioPlayer?.currentTime = TimeInterval(slider.value)
        audioPlayer?.prepareToPlay()
        audioPlayer?.play()
    }
    
    @objc func updateTime() {
        let currentTime = Int(audioPlayer?.currentTime ?? 0)
        let duration = Int(audioPlayer?.duration ?? 0)
        let total = currentTime - duration
        _ = String(total)
        
        let minutes = currentTime/60
        let seconds = currentTime - minutes / 60
        
        playedTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
    }
    
    @IBAction func songSliderAction(_ sender: Any) {
        audioPlayer?.stop()
        audioPlayer?.currentTime = TimeInterval(slider.value)
        audioPlayer?.prepareToPlay()
        audioPlayer?.play()
        
    }
    @IBAction func playPauseButton(sender: AnyObject) {
        //1 = play
        //2 = pausw
      
        //var playBtn = sender as! UIButton
        if toggleState == 1 {
            if let t = self.currentTime{
                audioPlayer?.stop()
                audioPlayer?.currentTime = t
                audioPlayer?.prepareToPlay()
                audioPlayer?.play()
            }else{
                currentTime = 0.0
                DispatchQueue.main.async {
                    self.playSound(url: self.mergeAudioURL as URL)
                }
            }
            
            toggleState = 2
        } else {
            audioPlayer?.pause()
            currentTime = audioPlayer?.currentTime
            
            toggleState = 1
        }
    }
    
    @IBAction func shareBtnTapped(_ sender: Any) {
//        let activityItem = URL.init(fileURLWithPath: Bundle.main.path(forResource: "fileName", ofType: "m4a")!)
        let items = [mergeAudioURL]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
    func playSound(url: URL) {
          // let url = NSBundle.mainBundle().URLForResource("soundName", withExtension: "mp3")!
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            guard let player = audioPlayer else { return }
            player.currentTime = self.currentTime ?? 0.0
            let session = AVAudioSession.sharedInstance()
            try session.setCategory(AVAudioSession.Category.playback)
            try session.setActive(true)
            player.prepareToPlay()
            
            player.play()
            
        } catch let error as NSError {
            let alert = UIAlertController(title: "Alert", message: "Recording doesn't exists on this device", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                  switch action.style{
                  case .default:
                        print("default")

                  case .cancel:
                        print("cancel")

                  case .destructive:
                        print("destructive")
                 
                  }}))
            
            self.present(alert, animated: true, completion: nil)
            
            print(error.description)
            
        }
    }
    @objc func updateSlider() {
        slider.value = Float(audioPlayer?.currentTime ?? 0)
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("Hellooooo")
        super.viewWillAppear(true)
    }
    
    
    @IBAction func backBtnTapped(_ sender: Any) {
        audioPlayer?.stop()
        

        if(isFromSaved ?? false){
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBar_ViewController") as? TabBar_ViewController
            vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(vc!, animated: true, completion: nil)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        

    }
    func updateSongLyrics(){
        var lyricsArr: Array<String> = []
        lyricsArr.append(songDataDict!["1st verse_lyrics"] as! String)
        lyricsArr.append("\n")
        lyricsArr.append(songDataDict!["b section_lyrics"] as! String)
        lyricsArr.append("\n")
        lyricsArr.append(songDataDict!["hook_lyrics"] as! String)
        lyricsArr.append("\n")
        lyricsArr.append(songDataDict!["2nd verse_lyrics"] as! String)
        lyricsArr.append("\n")
        lyricsArr.append(songDataDict!["b section_lyrics"] as! String)
        lyricsArr.append("\n")
        lyricsArr.append(songDataDict!["hook_lyrics"] as! String)
        lyricsArr.append("\n")
        lyricsArr.append(songDataDict!["bridge_lyrics"] as! String)
        lyricsArr.append("\n")
        lyricsArr.append(songDataDict!["hook_lyrics"] as! String)
        

        var stringUpdate = ""
        for txt in lyricsArr{
            stringUpdate = stringUpdate + txt
        }
        lyricsTxt.text = stringUpdate
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    func mergeAudioFiles(audioFileUrls: NSArray,title:String) -> String{
        let composition = AVMutableComposition()
        
        for i in 0 ..< audioFileUrls.count {
            
            let compositionAudioTrack :AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
            
            if(audioFileUrls[i] != nil){
                let asset = AVURLAsset(url: audioFileUrls[i] as! URL)
            
            let track = asset.tracks(withMediaType: AVMediaType.audio)[0]
            
            let timeRange = CMTimeRange(start: CMTimeMake(value: 0, timescale: 600), duration: track.timeRange.duration)
            
            try! compositionAudioTrack.insertTimeRange(timeRange, of: track, at: composition.duration)
            }
        }
        
        let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
        self.mergeAudioURL = documentDirectoryURL.appendingPathComponent("\(title + String(NSTimeIntervalSince1970)).m4a")! as URL as NSURL
        
        let assetExport = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetAppleM4A)
        assetExport?.outputFileType = AVFileType.m4a
        assetExport?.outputURL = mergeAudioURL as URL
        assetExport?.exportAsynchronously(completionHandler:
        {
                switch assetExport!.status
                {
                case AVAssetExportSessionStatus.failed:
                    print("failed \(String(describing: assetExport?.error))")
                case AVAssetExportSessionStatus.cancelled:
                    print("cancelled \(String(describing: assetExport?.error))")
                case AVAssetExportSessionStatus.unknown:
                    print("unknown\(String(describing: assetExport?.error))")
                case AVAssetExportSessionStatus.waiting:
                    print("waiting\(String(describing: assetExport?.error))")
                case AVAssetExportSessionStatus.exporting:
                    print("exporting\(String(describing: assetExport?.error))")
                default:
                    print("Audio Concatenation Complete")
                }
        })
        return self.mergeAudioURL.absoluteString!
    }

}
extension SongPlayer_ViewController {
    func setupNotifications() {
        // Get the default notification center instance.
        let nc = NotificationCenter.default
        nc.addObserver(self,
                       selector: #selector(handleRouteChange),
                       name: AVAudioSession.routeChangeNotification,
                       object: nil)
    }
    @objc func handleRouteChange(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let reasonValue = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt,
            let reason = AVAudioSession.RouteChangeReason(rawValue: reasonValue) else {
                return
        }
        
        // Switch over the route change reason.
        switch reason {

        case .newDeviceAvailable: // New device found.
            let session = AVAudioSession.sharedInstance()
            headphonesConnected = hasHeadphones(in: session.currentRoute)
        
        case .oldDeviceUnavailable: // Old device removed.
            if let previousRoute =
                userInfo[AVAudioSessionRouteChangePreviousRouteKey] as? AVAudioSessionRouteDescription {
                headphonesConnected = hasHeadphones(in: previousRoute)
            }
        
        default: ()
        }
    }

    func hasHeadphones(in routeDescription: AVAudioSessionRouteDescription) -> Bool {
        // Filter the outputs to only those with a port type of headphones.
        return !routeDescription.outputs.filter({$0.portType == .headphones}).isEmpty
    }
}


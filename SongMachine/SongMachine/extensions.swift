//
//  extensions.swift
//  SongMachine
//
//  Created by Technoventive on 01/12/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import Foundation
import UIKit

class BorderedTextField : UITextField {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        self.layer.cornerRadius = 9.0
        self.layer.borderWidth = 2
        self.backgroundColor = UIColor(r: 73, g: 73, b: 90)
        
        self.layer.borderColor = UIColor(r: 252, g: 154, b: 107).cgColor
        self.layer.masksToBounds = true
        self.font = UIFont(name: "Aileron-Regular", size: 18)
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor(r: 112, g: 112, b: 112)])
    }
    let padding = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
class BorderedTextView : UITextView , UITextViewDelegate{
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        //self.frame.size = CGSize(width: 340, height: 60)
        self.backgroundColor = UIColor(r: 73, g: 73, b: 90)
        self.layer.cornerRadius = 9.0
        self.layer.borderWidth = 0
        self.backgroundColor = UIColor(r: 73, g: 73, b: 90)
        
        self.layer.borderColor = UIColor(r: 252, g: 154, b: 107).cgColor
        self.layer.masksToBounds = true
    }
    let padding = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 5)
    
    
}
class ButtonExtension : UIButton{
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        //self.frame.size = CGSize(width: 340, height: 60)
        self.backgroundColor = UIColor(r: 73, g: 73, b: 90)
        self.layer.cornerRadius = 9.0
        self.layer.borderWidth = 2
        self.backgroundColor = UIColor(r: 73, g: 73, b: 90)
        
        self.layer.borderColor = UIColor(r: 151, g: 151, b: 151).cgColor
        self.layer.masksToBounds = true
    }
}
class ButtonExtension2 : UIButton{
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        //self.frame.size = CGSize(width: 340, height: 60)
        self.backgroundColor = UIColor(r: 73, g: 73, b: 90)
        self.layer.cornerRadius = 9.0
        self.layer.borderWidth = 2
        self.backgroundColor = UIColor(r: 73, g: 73, b: 90)
        
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.masksToBounds = true
    }
}
class bmp_DropDown : UITextField{
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        let c = Colors.init()
        
        self.layer.addSublayer(c.gl)
        //self.backgroundColor = UIColor(r: 250, g: 112, b: 154)
        self.layer.cornerRadius = 10.0
    }
    override func caretRect(for position: UITextPosition) -> CGRect {
        return CGRect.zero
    }
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)

        override open func textRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.inset(by: padding)
        }

        override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.inset(by: padding)
        }

        override open func editingRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.inset(by: padding)
        }
}
class BorderedDropdownTextField : UITextField{
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        self.layer.cornerRadius = 9.0
        self.layer.borderWidth = 2
        self.backgroundColor = UIColor(r: 73, g: 73, b: 90)
        
        self.layer.borderColor = UIColor(r: 252, g: 154, b: 107).cgColor
        self.layer.masksToBounds = true
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor(r: 112, g: 112, b: 112)])
    }
    let padding = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 5)
    override func caretRect(for position: UITextPosition) -> CGRect {
        return CGRect.zero
    }
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override func shouldChangeText(in range: UITextRange, replacementText text: String) -> Bool {
        return false
    }
    
}


extension UITextField {
    func setIcon(_ image: UIImage) {
       let iconView = UIImageView(frame:
                      CGRect(x: -5, y: 0, width: 13, height: 8))
       iconView.image = image
       let iconContainerView: UIView = UIView(frame:
                      CGRect(x: 0, y: 0, width: 12, height: 7))
       iconContainerView.addSubview(iconView)
        rightView = iconContainerView
       rightViewMode = .always
    }
}
class Colors {
    var gl:CAGradientLayer!

    init() {
        let colorTop = UIColor(red: 254 / 255.0, green: 193 / 255.0, blue: 64 / 255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 250 / 255.0, green: 112 / 255.0, blue: 154 / 255.0, alpha: 1.0).cgColor

        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop,colorBottom]
        self.gl.locations = [0.0, 1.0]
    }
}

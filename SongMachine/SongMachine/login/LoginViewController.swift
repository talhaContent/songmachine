//
//  LoginViewController.swift
//  SongMachine
//
//  Created by SAM on 11/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit
import AuthenticationServices

@available(iOS 13.0, *)
class LoginViewController: UIViewController {

    var semaphore = DispatchSemaphore (value: 0)

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var emailTxtField: TextField!
    @IBOutlet var passwordTxtField: TextField!
    @IBOutlet var appleBtn: UIButton!
    let appleProvider = SignInAppleClient()
    var mainView_YAxis: CGFloat = 0.0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
        
        hideKeyboard()
        mainView_YAxis = mainView.frame.origin.y

        scrollView.contentSize = CGSize(width: scrollView.frame.size.width, height: appleBtn.frame.origin.y+appleBtn.frame.size.height);

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        self.appleBtn.addTarget(self, action: #selector(signInWithApplication(sender:)), for: .touchUpInside)
        
        
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    

    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            if(emailTxtField.isFirstResponder == true){
                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight - (emailTxtField.frame.origin.y + emailTxtField.frame.size.height)
        }
            else{
                 mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight - (passwordTxtField.frame.origin.y + passwordTxtField.frame.size.height)
            }
        }
//        scrollView.frame = CGRect(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height-220);
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
//
            if(emailTxtField.isFirstResponder == true || passwordTxtField.isFirstResponder == true){
                mainView.frame.origin.y = mainView_YAxis
            }
//            if(emailTxtField.isFirstResponder == true){
//                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight + emailTxtField.frame.size.height
//            }
//            else{
//                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight + passwordTxtField.frame.size.height
//            }
        }
//        scrollView.frame = CGRect(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height+220);
    }
    func hideKeyboard(){
        let bar = UIToolbar()
          let reset = UIBarButtonItem(title: "Hide Keyboard", style: .plain, target: self, action: #selector(hideTapped))
        bar.items = [reset]
        bar.sizeToFit()
        emailTxtField.inputAccessoryView = bar
        passwordTxtField.inputAccessoryView = bar
//        appleBtn.inputAccessoryView = bar

          
          
      }
      @objc func hideTapped(sender: UIBarButtonItem){
          self.view.endEditing(true)
      }
      
    @objc func signInWithApplication(sender: UIButton){
        if #available(iOS 13.0, *) {
            appleProvider.handleAppleIdRequest(block: { fullName, email, token in
                // receive data in login class.
                UserDefaults.standard.set(fullName, forKey: "username")
                UserDefaults.standard.set(email, forKey: "email")
                
                 UserDefaults.standard.set("1", forKey: "userId")
                UserDefaults.standard.set(true, forKey: "LoggedIn")
                
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func signInWithPhoneNumberTapped(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Main_PhoneNumber_ViewController") as? Main_PhoneNumber_ViewController
        //        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //        vc!.songDataDict = dict
        //        self.navigationController?.pushViewController(vc!, animated: true)
        self.present(vc!, animated: true, completion: nil)
    }
    @IBAction func loginTapped(_ sender: Any) {
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBar_ViewController") as? TabBar_ViewController
//        //        self.navigationController?.pushViewController(vc!, animated: true)
//        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        //        vc!.songDataDict = dict
//        //        self.navigationController?.pushViewController(vc!, animated: true)
//        self.present(vc!, animated: true, completion: nil)
        
        if(emailTxtField.text?.trimmingCharacters(in: CharacterSet.whitespaces) != "" || passwordTxtField.text?.trimmingCharacters(in: CharacterSet.whitespaces) != ""){
            callLoginService()
        }
        else{
            self.showAlert(title: "Login Failed", msg: "Please enter username/password.")
        }
     

    }
    
//    func callService(){
//
//
//        let parameters = [
//          [
//            "key": "type",
//            "value": "login",
//            "type": "text"
//          ],
//          [
//            "key": "email",
//            "value": "abc1237",
//            "type": "text"
//          ],
//          [
//            "key": "password",
//            "value": "123456",
//            "type": "text"
//          ]] as [[String : Any]]
//
//        let boundary = "Boundary-\(UUID().uuidString)"
//        var body = ""
//        var error: Error? = nil
//        for param in parameters {
//          if param["disabled"] == nil {
//            let paramName = param["key"]!
//            body += "--\(boundary)\r\n"
//            body += "Content-Disposition:form-data; name=\"\(paramName)\""
//            let paramType = param["type"] as! String
//            if paramType == "text" {
//              let paramValue = param["value"] as! String
//              body += "\r\n\r\n\(paramValue)\r\n"
//            } else {
//              let paramSrc = param["src"] as! String
//              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
//                let fileContent = String(data: fileData!, encoding: .utf8)!
//              body += "; filename=\"\(paramSrc)\"\r\n"
//                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
//            }
//          }
//        }
//        body += "--\(boundary)--\r\n";
//        let postData = body.data(using: .utf8)
//
//        var request = URLRequest(url: URL(string: "http://7tracking.com/api/api.php")!,timeoutInterval: Double.infinity)
//        request.addValue("recent_songs", forHTTPHeaderField: "type")
//        request.addValue("1", forHTTPHeaderField: "user_id")
//        request.addValue("PHPSESSID=n18qits4o0v908r7c4kusse4d2", forHTTPHeaderField: "Cookie")
//        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//
//        request.httpMethod = "POST"
//        request.httpBody = postData
//
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//          guard let data = data else {
//            print(String(describing: error))
//            return
//          }
//          print(String(data: data, encoding: .utf8)!)
//            do {
//                // make sure this JSON is in the format we expect
//                if var json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
//                    print(json)
//                    if(json != nil){
//                        let userid = json["userid"] as? String
//                        let message = json["message"] as? String
//                        UserDefaults.standard.set(userid, forKey: "userId")
//                        if((message?.elementsEqual("Login Successfully"))!){
//
//                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBar_ViewController") as? TabBar_ViewController
//                            //        self.navigationController?.pushViewController(vc!, animated: true)
//                            vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                            //        vc!.songDataDict = dict
//                            //        self.navigationController?.pushViewController(vc!, animated: true)
//                            self.present(vc!, animated: true, completion: nil)
//                        }
//
//                    }
//                    // try to read out a string array
//                    //                    if let names = json["result"] as? [String] {
//                    //                        let message = json["message"]
//                    //                        print(names)
//                    //                    }
//                }
//            } catch let error as NSError {
//                print("Failed to load: \(error.localizedDescription)")
//            }
//
//
//
//            self.semaphore.signal()
//        }
//
//        task.resume()
//        semaphore.wait()
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    @IBAction func forgetPasswordBtnTapped(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgetPassword_ViewController") as? ForgetPassword_ViewController
        //        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //        vc!.songDataDict = dict
        //        self.navigationController?.pushViewController(vc!, animated: true)
        self.present(vc!, animated: true, completion: nil)
    }
    //
//    }
//
//
    
    
    func callLoginService(){
        
        
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)

        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        let parameters = [
             [
               "key": "type",
               "value": "login",
               "type": "text"
             ],
             [
               "key": "email",
               "value": emailTxtField!.text,
               "type": "text"
             ],
             [
               "key": "password",
               "value": passwordTxtField!.text,
               "type": "text"
             ]] as [[String : Any]]

             let boundary = "Boundary-\(UUID().uuidString)"
             var body = ""
        var _: Error? = nil
             for param in parameters {
               if param["disabled"] == nil {
                 let paramName = param["key"]!
                 body += "--\(boundary)\r\n"
                 body += "Content-Disposition:form-data; name=\"\(paramName)\""
                 let paramType = param["type"] as! String
                 if paramType == "text" {
                   let paramValue = param["value"] as! String
                   body += "\r\n\r\n\(paramValue)\r\n"
                 } else {
                   let paramSrc = param["src"] as! String
                    let fileData = (try? NSData(contentsOfFile:paramSrc, options:[]) as Data)!
                   let fileContent = String(data: fileData, encoding: .utf8)!
                   body += "; filename=\"\(paramSrc)\"\r\n"
                     + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                 }
               }
             }
             body += "--\(boundary)--\r\n";
             let postData = body.data(using: .utf8)

             var request = URLRequest(url: URL(string: "https://7tracking.com/api/api.php")!,timeoutInterval: Double.infinity)
             request.addValue("PHPSESSID=n18qits4o0v908r7c4kusse4d2", forHTTPHeaderField: "Cookie")
             request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

             request.httpMethod = "POST"
             request.httpBody = postData

             let task = URLSession.shared.dataTask(with: request) { data, response, error in
               guard let data = data else {
                 print(String(describing: error))
                 return
               }
               print(String(data: data, encoding: .utf8)!)

                
                DispatchQueue.main.async {

                
                do {
                    // make sure this JSON is in the format we expect
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        print(json)
                        let message = json["message"] as? String
                        if((message?.lowercased().elementsEqual("login successfully"))!){
                            
                            if(json["login"] != nil){
                                let login = json["login"] as! Array<Dictionary<String,Any>>
                                let userId = login[0]
                                UserDefaults.standard.set(userId["login"], forKey: "userId")
                                UserDefaults.standard.set(userId["username"], forKey: "username")
                                UserDefaults.standard.set(userId["email"], forKey: "email")
                                UserDefaults.standard.set(true, forKey: "LoggedIn")
                            }
                                alert.dismiss(animated: true) {
                                self.moveToNextController()
                            }
                        }
                        else{
                            alert.dismiss(animated: true) {
                                self.showAlert(title: "Login Failed", msg: "Please enter correct username/password.")
                            }
                          
                        }
                    }
                } catch let error as NSError {
                    alert.dismiss(animated: true) {
                        self.showAlert(title: "Login Failed", msg: (error.localizedDescription))
                    }
                    print("Failed to load: \(error.localizedDescription)")
                }
                }

                
                
                self.semaphore.signal()

             }

             task.resume()
             semaphore.wait()
    }
    
    func moveToNextController(){
        
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBar_ViewController") as? TabBar_ViewController
        //        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //        vc!.songDataDict = dict
        //        self.navigationController?.pushViewController(vc!, animated: true)
        self.present(vc!, animated: true, completion: nil)
    }
    
    
    func showAlert(title:String,msg:String){
          let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
          alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
          self.present(alert, animated: true, completion: nil)
      }
      
    @IBAction func backBtnTapped(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Main_PhoneNumber_ViewController") as? Main_PhoneNumber_ViewController
               //        self.navigationController?.pushViewController(vc!, animated: true)
               vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
               //        vc!.songDataDict = dict
               //        self.navigationController?.pushViewController(vc!, animated: true)
               self.present(vc!, animated: true, completion: nil)
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



@available(iOS 13.0, *)
extension LoginViewController: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
         self.view.endEditing(true)
     }
}

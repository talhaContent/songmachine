//
//  Main_PhoneNumber_ViewController.swift
//  SongMachine
//
//  Created by SAM on 11/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

protocol PassDataDelegate {
    func passPhoneNumber(num:String?)
}



class Main_PhoneNumber_ViewController: UIViewController, UITextFieldDelegate {

    var semaphore = DispatchSemaphore (value: 0)

    
    @IBOutlet var mainView: UIView!
    @IBOutlet var mobileNum_TxtField: TextField!
    var delegate: PassDataDelegate!
    
    override func viewDidLayoutSubviews() {
        self.navigationController?.navigationBar.isHidden = false
//        self.navigationController?.navigationBar.backgroundColor = .red
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "<", style: .plain, target: self, action: nil)
//        //          self.navigationController?.view.tintColor = UIColor.orange
//        self.navigationItem.backBarButtonItem?.title = "back"
//        //For back button in navigation bar
//        let backButton = UIBarButtonItem()
//        backButton.title = "back"
        
//        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == mobileNum_TxtField {
               let allowedCharacters = CharacterSet(charactersIn:"+0123456789 ")//Here change this characters based on your requirement
               let characterSet = CharacterSet(charactersIn: string)
               return allowedCharacters.isSuperset(of: characterSet)
           }
        
        if range.length>0  && range.location == 0 {
            let changedText = NSString(string: textField.text!).substring(with: range)
            if changedText.contains("+1") {
                 return false
             }
         }
        
        if(!(textField.text?.contains("+1"))!){
            textField.text = "+1\(String(describing: textField.text))"
        }
    
        
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        showLoader()
        mobileNum_TxtField.delegate = self
        hideKeyboard()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )

        // Do any additional setup after loading the view.
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight - 250
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight - 20
        }
    }
    func hideKeyboard(){
      let bar = UIToolbar()
        let reset = UIBarButtonItem(title: "Hide Keyboard", style: .plain, target: self, action: #selector(hideTapped))
        bar.items = [reset]
        bar.sizeToFit()
        mobileNum_TxtField.inputAccessoryView = bar
        
        
    }
    
    
    @IBAction func signInUsingEmailTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        pvc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(pvc, animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "ViewController")
        pvc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(pvc, animated: true, completion: nil)
    }
    @objc func hideTapped(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    

    func showAlert(title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func getStarted(_ sender: Any) {
        let num = mobileNum_TxtField.text
        if((num?.isValidPhone())! == true){
        //self.callPhoneNumService(phoneNum: num!)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
              let pvc = storyboard.instantiateViewController(withIdentifier: "OTP_ViewController") as! OTP_ViewController
              pvc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
              pvc.phoneNum = num!
              self.present(pvc, animated: true, completion: nil)
//            self.delegate = pvc as! PassDataDelegate
//            if(self.delegate != nil){
//                     self.delegate.passPhoneNumber(num: num)
//            }
        }
        else{
            let alert = UIAlertController(title: "Wrong Phone Number", message: "Please Enter Correct Phone Number.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
 
    
    
    func showLoader(){
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
    }


    func textFieldDidBeginEditing(_ textField: UITextField) {
        mainView.frame.origin.y = mainView.frame.origin.y
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
              self.view.endEditing(true)
          }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
}
extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }

    // verify Valid PhoneNumber or Not
    func isValidPhone() -> Bool {

        let phoneRegex = "^((\\+)|(00))[0-9]{6,12}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: self)
        return valid
    }
   
}


//extension Main_PhoneNumber_ViewController: UITextFieldDelegate{
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        self.view.endEditing(true)
//    }
//}

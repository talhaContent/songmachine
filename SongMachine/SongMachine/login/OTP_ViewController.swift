//
//  OTP_ViewController.swift
//  SongMachine
//
//  Created by SAM on 14/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseCore

typealias CompletionHandler = (_ success:Bool) -> Void
class OTP_ViewController: UIViewController, PassDataDelegate, UITextFieldDelegate {
    
    
    var semaphore = DispatchSemaphore (value: 0)

    func passPhoneNumber(num: String?) {
//        PhoneAuthProvider.provider().verifyPhoneNumber(num!, uiDelegate: nil) { (verificationID, error) in
//                     if let error = error {
//                       //self.showMessagePrompt(error.localizedDescription)
//                       return
//                     }
//        }

    }
    
 
    @IBOutlet var one: UITextField!
    @IBOutlet var two: UITextField!
    @IBOutlet var three: UITextField!
    @IBOutlet var four: UITextField!
    @IBOutlet var five: UITextField!
    @IBOutlet var six: UITextField!
    
    
    var phoneNum: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authPhoneNum()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
        //addAccessoryView()
        addKeyboardToolbar()
        one.delegate = self
        two.delegate = self
        three.delegate = self
        four.delegate = self
        five.delegate = self
        six.delegate = self


        one.becomeFirstResponder()
//        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNum, uiDelegate: nil) { (verificationID, error) in
//          if let error = error {
//            //self.showMessagePrompt(error.localizedDescription)
//            return
//          }
//          // Sign in using the verificationID and the code sent to the user
//          // ...
//        }
    }
    @objc func dismissKeyboard(){
         self.view.endEditing(true)
     }
    func authPhoneNum(){
        
        //phoneNum.trimmingCharacters(in: CharacterSet.whitespaces)
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNum, uiDelegate: nil) { (verificationID, error) in
            print(verificationID)
            print(error)
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")

            if let error = error {
                //self.showMessagePrompt(error.localizedDescription)
                return
            }
        }
        
    }
    
   @objc func doneTapped(){
    
 
    }
    
    func verifyPhoneNumberAlert(code:String) {
//        let alert = UIAlertController(title: "Verify Phone", message: "Please enter verification code", preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "Ok", style: .default) { action in
//          guard let code = alert.textFields?.first?.text, code != ""     else {
//          return
//     }
        if(code.count == 6){
            let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID!, verificationCode: code)
            Auth.auth().signInAndRetrieveData(with: credential,completion: { (authResult, error) in
                
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    self.callPhoneNumService(phoneNum: self.phoneNum)
                    
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let pvc = storyboard.instantiateViewController(withIdentifier: "Register_ViewController")
//                    pvc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                    self.present(pvc, animated: true, completion: nil)
                    
                    print(authResult?.credential.debugDescription as Any)
                }
                
            })
        }
        else{
            showAlert(title:"Authentication", msg: "Incorrect Code")
        }
//       alert.addTextField { textfield in
//       textfield.keyboardType = .phonePad
//       textfield.textContentType = .oneTimeCode
//       textfield.placeholder = "Enter code"
//       textfield.delegate = self
//      }
//      alert.addAction(okAction)
//      present(alert, animated: true, completion: nil)
    }
    func showAlert(title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func resendCodeTapped(_ sender: Any) {
        authPhoneNum()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    func callPhoneNumService(phoneNum:String){

        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)

             alert.view.tintColor = UIColor.black
             let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
             loadingIndicator.hidesWhenStopped = true
             loadingIndicator.style = UIActivityIndicatorView.Style.gray
             loadingIndicator.startAnimating();

             alert.view.addSubview(loadingIndicator)
             present(alert, animated: true, completion: nil)
        
            let parameters = [
              [
                "key": "type",
                "value": "phone_login",
                "type": "text"
              ],
              [
                "key": "phone",
                "value": "\(phoneNum)",
                "type": "text"
              ]] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                    let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)

            var request = URLRequest(url: URL(string: "https://7tracking.com/api/api.php")!,timeoutInterval: Double.infinity)
            request.addValue("PHPSESSID=n18qits4o0v908r7c4kusse4d2", forHTTPHeaderField: "Cookie")
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "POST"
            request.httpBody = postData

            
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                return
              }
              print(String(data: data, encoding: .utf8)!)
                DispatchQueue.main.async {
                do {
                    // make sure this JSON is in the format we expect
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        let message = json["message"] as? String
                        let result = json["result"] as? Int
                        if((message?.elementsEqual("Registered Successfully!"))! && result == 1){
                            alert.dismiss(animated: true, completion: nil)
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let pvc = storyboard.instantiateViewController(withIdentifier: "Register_ViewController")
                            pvc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            self.present(pvc, animated: true, completion: nil)
//                            self.showAlert(title: "Create Account", msg: "Registered Successfully!")
                        }
                    }
                } catch let error as NSError {
                    alert.dismiss(animated: true, completion: nil)
                    self.showAlert(title: "Create Account", msg: "Registered Unsuccessfully!")
                    print("Failed to load: \(error.localizedDescription)")
                }
                }
                //self.semaphore.signal()
            }
            task.resume()
            //semaphore.wait()

        }
    
    
    func addAccessoryView(){
      let bar = UIToolbar()
        let reset = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneTapped))
        bar.items = [reset]
        bar.sizeToFit()
        one.inputAccessoryView = bar
        two.inputAccessoryView = bar
        three.inputAccessoryView = bar
        four.inputAccessoryView = bar

        
        
    }
    func addKeyboardToolbar()  {
        let ViewForDoneButtonOnKeyboard = UIToolbar()
        ViewForDoneButtonOnKeyboard.sizeToFit()
        let button = UIButton.init(type: .custom)
//        button.setImage(UIImage.init(named: "login-logo"), for: UIControl.State.normal)
        button.setTitle("Done", for: .normal)
        button.addTarget(self, action:#selector(doneBtnfromKeyboardClicked), for:.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width:UIScreen.main.bounds.width, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        ViewForDoneButtonOnKeyboard.items = [barButton]
        one.inputAccessoryView = ViewForDoneButtonOnKeyboard
        two.inputAccessoryView = ViewForDoneButtonOnKeyboard
        three.inputAccessoryView = ViewForDoneButtonOnKeyboard
        four.inputAccessoryView = ViewForDoneButtonOnKeyboard
        five.inputAccessoryView = ViewForDoneButtonOnKeyboard
        six.inputAccessoryView = ViewForDoneButtonOnKeyboard


    }


    @objc func doneBtnfromKeyboardClicked (){
        
        let code = "\(one.text!)\(two.text!)\(three.text!)\(four.text!)\(five.text!)\(six.text!)"
        verifyPhoneNumberAlert(code:code)
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
    
        if(string.count == 0){
            return true
        }
        
        
        if(textField.text!.count >= 1){
        return false
        }
        
        if(textField.tag == 1){
            textField.text = string
            two.becomeFirstResponder()
            return true
        }
        else if(textField.tag == 2){
            textField.text = string
            three.becomeFirstResponder()
            return true
        }
        else if(textField.tag == 3){
            textField.text = string
            four.becomeFirstResponder()
            return true
        }
        else if(textField.tag == 4){
            textField.text = string
            five.becomeFirstResponder()
            return true
        }
        else if(textField.tag == 5){
            textField.text = string
            six.becomeFirstResponder()
            return true
        }
        
        return true
    }
    @IBAction func backBtnTapped(_ sender: Any) {
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let pvc = storyboard.instantiateViewController(withIdentifier: "Main_PhoneNumber_ViewController")
         pvc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
         self.present(pvc, animated: true, completion: nil)
     }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

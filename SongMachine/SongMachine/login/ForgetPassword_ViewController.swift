//
//  ForgetPassword_ViewController.swift
//  SongMachine
//
//  Created by SAM on 30/10/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

class ForgetPassword_ViewController: UIViewController {
   var semaphore = DispatchSemaphore (value: 0)
    @IBOutlet var emailTxtField: TextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard(){
         self.view.endEditing(true)
     }
    @IBAction func sendEmailTapped(_ sender: Any) {
        
        
        if(!(emailTxtField.text?.elementsEqual(""))!){
         
            let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
            
            alert.view.tintColor = UIColor.black
            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.gray
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            present(alert, animated: true, completion: nil)

            let parameters = [
              [
                "key": "type",
                "value": "password_forget",
                "type": "text"
              ],
              [
                "key": "email",
                "value": emailTxtField.text,
                "type": "text"
              ]] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                    let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)

            var request = URLRequest(url: URL(string: "https://7tracking.com/api/api.php")!,timeoutInterval: Double.infinity)
            request.addValue("recent_songs", forHTTPHeaderField: "type")
            request.addValue("1", forHTTPHeaderField: "user_id")
            request.addValue("PHPSESSID=n18qits4o0v908r7c4kusse4d2", forHTTPHeaderField: "Cookie")
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "POST"
            request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                return
              }
              print(String(data: data, encoding: .utf8)!)
                DispatchQueue.main.async {

                do {
                    // make sure this JSON is in the format we expect
                    if var json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        print(json)
                        if(json != nil){
                            //                            let loginDataResponse = json["login"] as! Array<String,Any>
                            //                            UserDefaults.standard.set(loginDataResponse["login"], forKey: "userId")
                            //                            let str = UserDefaults.standard.string(forKey: "userId")
                            //                            for index in login as! Dictionary<String,Any>{
                            //let indexx = login[0]
                            //                            }
                            //let indexValue = login["0"]
                            let message = json["message"] as? String
                            // UserDefaults.standard.set(indexx, forKey: "userId")
                            if((message?.lowercased().elementsEqual("password sent"))!){
                                
                                alert.dismiss(animated: true, completion: nil)
                                    self.showAlert(title: "Sent", msg: "Please check your email.")
                                    
                                    
                                
                            }
                            else{
                                alert.dismiss(animated: true, completion: nil)
                                self.showAlert(title: "Update failed", msg: "Please enter correct password.")
                            }
                            
                        }
                        // try to read out a string array
                        //                    if let names = json["result"] as? [String] {
                        //                        let message = json["message"]
                        //                        print(names)
                        //                    }
                    }
                } catch let error as NSError {
                    alert.dismiss(animated: true) {
                        self.showAlert(title: "Update failed", msg: error.localizedDescription)
                        
                    }
                    print("Failed to load: \(error.localizedDescription)")
                }
                               
                }
                
                
                self.semaphore.signal()
            }

            task.resume()
            semaphore.wait()

        }
        
        
    }
    
    func moveToNextController(){
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            //        self.navigationController?.pushViewController(vc!, animated: true)
            vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            //        vc!.songDataDict = dict
            //        self.navigationController?.pushViewController(vc!, animated: true)
            self.present(vc!, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }

    }
    
    
    func showAlert(title:String,msg:String){
          let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            self.moveToNextController()
        }))
          self.present(alert, animated: true, completion: nil)
      }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        pvc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(pvc, animated: true, completion: nil)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

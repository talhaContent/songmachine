//
//  EditProfile_ViewController.swift
//  SongMachine
//
//  Created by SAM on 31/10/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

class EditProfile_ViewController: UIViewController {

    
    var semaphore = DispatchSemaphore (value: 0)
    @IBOutlet var password: TextField!
    @IBOutlet var email: TextField!
    @IBOutlet var name: TextField!
    @IBOutlet var mainView: UIView!
    var mainYAxis: CGFloat = 0.0
    var toggleState = 0
    @IBOutlet weak var editButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboard()
        setFields()
        mainYAxis = mainView.frame.origin.y
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    func setFields(){
        let nameTxt = UserDefaults.standard.string(forKey: "username")!
        let emailTxt = UserDefaults.standard.string(forKey: "email")!
        self.name.text = nameTxt
        self.email.text = emailTxt
        self.name.isEnabled = false
        self.email.isEnabled = false
        self.password.isEnabled = false
        toggleState = 0
        self.editButton.setTitle("Edit", for: .normal)
    }
    
    @objc func dismissKeyboard(){
         self.view.endEditing(true)
     }
    
    func verified() -> Bool{
        
        if(name.text != "" && password.text != "" && email.text != ""){
            return true
        }
        
        return false
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        //self.moveToNextController()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
//            if(email.isFirstResponder == true){
//                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight - (email.frame.origin.y + email.frame.size.height)
//            }
             if(password.isFirstResponder == true){
                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight - (password.frame.origin.y + password.frame.size.height)
            }
        }
      }
      
      @objc func keyboardWillHide(_ notification: Notification) {
          if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
              let keyboardRectangle = keyboardFrame.cgRectValue
              let keyboardHeight = keyboardRectangle.height
              mainView.frame.origin.y = mainYAxis
          }
      }
    func hideKeyboard(){
        let bar = UIToolbar()
        let reset = UIBarButtonItem(title: "Hide Keyboard", style: .plain, target: self, action: #selector(hideTapped))
        bar.items = [reset]
        bar.sizeToFit()
        password.inputAccessoryView = bar
        email.inputAccessoryView = bar
        name.inputAccessoryView = bar
        
    }
    @objc func hideTapped(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @IBAction func editBtnTapped(_ sender: Any) {
        if(toggleState == 0){
            self.name.isEnabled = true
            self.email.isEnabled = true
            self.password.isEnabled = true
            self.editButton.setTitle("Update", for: .normal)
            toggleState = 1
        }else{
            if(verified() == true){

                
                let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
                
                alert.view.tintColor = UIColor.black
                let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
                loadingIndicator.hidesWhenStopped = true
                loadingIndicator.style = UIActivityIndicatorView.Style.gray
                loadingIndicator.startAnimating();
                
                alert.view.addSubview(loadingIndicator)
                present(alert, animated: true, completion: nil)
     
                let nameTxt = name.text!
                let emailTxt = email.text!
                let passwordTxt = password.text!
                let userID = UserDefaults.standard.string(forKey: "userId")!
                
                let parameters = [
                  [
                    "key": "type",
                    "value": "update",
                    "type": "text"
                  ],
                  [
                    "key": "username",
                    "value": nameTxt,
                    "type": "text"
                  ],
                  [
                    "key": "email",
                    "value": emailTxt,
                    "type": "text"
                  ],
                  [
                    "key": "password",
                    "value": passwordTxt,
                    "type": "text"
                  ],
                  [
                    "key": "user_id",
                    "value": userID,
                    "type": "text"
                  ],
                  [
                    "key": "gender",
                    "value": "male",
                    "type": "text"
                  ]] as [[String : Any]]

                let boundary = "Boundary-\(UUID().uuidString)"
                var body = ""
                var error: Error? = nil
                for param in parameters {
                  if param["disabled"] == nil {
                    let paramName = param["key"]!
                    body += "--\(boundary)\r\n"
                    body += "Content-Disposition:form-data; name=\"\(paramName)\""
                    let paramType = param["type"] as! String
                    if paramType == "text" {
                      let paramValue = param["value"] as! String
                      body += "\r\n\r\n\(paramValue)\r\n"
                    } else {
                      let paramSrc = param["src"] as! String
                      let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                        let fileContent = String(data: fileData!, encoding: .utf8)!
                      body += "; filename=\"\(paramSrc)\"\r\n"
                        + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                    }
                  }
                }
                body += "--\(boundary)--\r\n";
                let postData = body.data(using: .utf8)

                var request = URLRequest(url: URL(string: "https://7tracking.com/api/api.php")!,timeoutInterval: Double.infinity)
                request.addValue("recent_songs", forHTTPHeaderField: "type")
                request.addValue("1", forHTTPHeaderField: "user_id")
                request.addValue("PHPSESSID=n18qits4o0v908r7c4kusse4d2", forHTTPHeaderField: "Cookie")
                request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

                request.httpMethod = "POST"
                request.httpBody = postData

                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                  guard let data = data else {
                    print(String(describing: error))
                    return
                  }
                  print(String(data: data, encoding: .utf8)!)
                    DispatchQueue.main.async {

                    do {
                        // make sure this JSON is in the format we expect
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            print(json)
                            if(json != nil){
                                //                            let loginDataResponse = json["login"] as! Array<String,Any>
                                //                            UserDefaults.standard.set(loginDataResponse["login"], forKey: "userId")
                                //                            let str = UserDefaults.standard.string(forKey: "userId")
                                //                            for index in login as! Dictionary<String,Any>{
                                //let indexx = login[0]
                                //                            }
                                //let indexValue = login["0"]
                                let message = json["message"] as? String
                                // UserDefaults.standard.set(indexx, forKey: "userId")
                                if((message?.lowercased().elementsEqual("profile updated successfully"))!){
                                    
    //                                if(json["result"] != nil){
    //                                    let login = json["result"] as! Array<Dictionary<String,Any>>
                                        //let userId = login[0]
                                        UserDefaults.standard.set(userID, forKey: "userId")
                                        UserDefaults.standard.set(nameTxt, forKey: "username")
                                        UserDefaults.standard.set(emailTxt, forKey: "email")
    //
    //
    //                                    let str = UserDefaults.standard.string(forKey: "userId")
    //
    //
    //
    //                                }
    //                                DispatchQueue.main.async {
                                    alert.dismiss(animated: true) {
                                        
                                    
                                        self.showAlert(title: "Updated", msg: "Profile Updated Successfully.")
                                    }
                                        
    //                                }
                                }
                                else{
                                    alert.dismiss(animated: true) {
                                        
                                    
                                    self.showAlert(title: "Update failed", msg: "Please enter correct username/password.")
                                    }
                                }
                                
                            }
                            // try to read out a string array
                            //                    if let names = json["result"] as? [String] {
                            //                        let message = json["message"]
                            //                        print(names)
                            //                    }
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                    }
                    
                    
                    
                    self.semaphore.signal()
                }

                task.resume()
                semaphore.wait()

            }
            
            setFields()
        }
        
        
        
        
    }
    
    func moveToNextController(){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBar_ViewController") as? TabBar_ViewController
        //        self.navigationController?.pushViewController(vc!, animated: true)
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //        vc!.songDataDict = dict
        //        self.navigationController?.pushViewController(vc!, animated: true)
        self.present(vc!, animated: true, completion: nil)
    }
    
    
    func showAlert(title:String,msg:String){
          let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            self.moveToNextController()
        }))
          self.present(alert, animated: true, completion: nil)
      }
      
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

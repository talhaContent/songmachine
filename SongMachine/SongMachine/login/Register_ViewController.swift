//
//  Register_ViewController.swift
//  SongMachine
//
//  Created by SAM on 04/10/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

class Register_ViewController: UIViewController {

    
    @IBOutlet var mainView: UIView!
    @IBOutlet var nameTxtField: TextField!
    @IBOutlet var emailTxtField: TextField!
    @IBOutlet var passwordTxtField: TextField!
    @IBOutlet var confirmPasswordTxtField: TextField!
    var mainView_YAxis: CGFloat = 0.0
    
    
    var semaphore = DispatchSemaphore (value: 0)

    let parameters = [
      [
        "key": "type",
        "value": "register",
        "type": "text"
      ],
      [
        "key": "username",
        "value": "abc",
        "type": "text"
      ],
      [
        "key": "email",
        "value": "abc123",
        "type": "text"
      ],
      [
        "key": "password",
        "value": "123456",
        "type": "text"
      ]] as [[String : Any]]
    
    
    var registerParamArray:[[String : Any]]?

    let boundary = "Boundary-\(UUID().uuidString)"
    var body = ""
    var error: Error? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        self.view.addGestureRecognizer(tap)
        hideKeyboard()
        mainView_YAxis = mainView.frame.origin.y
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )

    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    @objc func dismissKeyboard(){
         self.view.endEditing(true)
     }
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
//            if(nameTxtField.isFirstResponder == true){
//                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight - nameTxtField.frame.origin.y
//            }
//            else if(emailTxtField.isFirstResponder == true){
//                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight - emailTxtField.frame.origin.y
//            }
             if(passwordTxtField.isFirstResponder == true){
                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight - (passwordTxtField.frame.origin.y + passwordTxtField.frame.size.height)
            }
            else if(confirmPasswordTxtField.isFirstResponder == true){
                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight - (confirmPasswordTxtField.frame.origin.y + confirmPasswordTxtField.frame.size.height)
            }
        }
        //        scrollView.frame = CGRect(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height-220);
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
             if(confirmPasswordTxtField.isFirstResponder == true || passwordTxtField.isFirstResponder == true){
            mainView.frame.origin.y = mainView_YAxis
            }
//            if(nameTxtField.isFirstResponder == true){
//                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight + nameTxtField.frame.size.height
//            }
//            else if(emailTxtField.isFirstResponder == true){
//                mainView.frame.origin.y = self.mainView.frame.size.height - keyboardHeight + emailTxtField.frame.size.height
//            }
//             if(passwordTxtField.isFirstResponder == true){
//                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight + (passwordTxtField.frame.size.height)
//            }
//            else if(confirmPasswordTxtField.isFirstResponder == true){
//                mainView.frame.origin.y = self.view.frame.size.height - keyboardHeight + (confirmPasswordTxtField.frame.size.height)
//            }
        }
        //        scrollView.frame = CGRect(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height+220);
    }
  
    func hideKeyboard(){
        let bar = UIToolbar()
        let reset = UIBarButtonItem(title: "Hide Keyboard", style: .plain, target: self, action: #selector(hideTapped))
        let next = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextTextField))
        bar.items = [reset,next]
        bar.sizeToFit()
        nameTxtField.inputAccessoryView = bar
        emailTxtField.inputAccessoryView = bar
        passwordTxtField.inputAccessoryView = bar
        confirmPasswordTxtField.inputAccessoryView = bar
        //bridge.inputAccessoryView = bar
        
    }
    
    @objc func nextTextField(sender: UIBarButtonItem){
        if(nameTxtField.isFirstResponder == true){
            emailTxtField.becomeFirstResponder()
        }
        else if(emailTxtField.isFirstResponder == true){
            passwordTxtField.becomeFirstResponder()
        }
        else if(passwordTxtField.isFirstResponder == true){
            confirmPasswordTxtField.becomeFirstResponder()
            
        }

    }
    
    @objc func hideTapped(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    func callRegisterService(paramArray:[[String : Any]]){
        
        
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)

             alert.view.tintColor = UIColor.black
             let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
             loadingIndicator.hidesWhenStopped = true
             loadingIndicator.style = UIActivityIndicatorView.Style.gray
             loadingIndicator.startAnimating();

             alert.view.addSubview(loadingIndicator)
             present(alert, animated: true, completion: nil)
        
        for param in paramArray {
            if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                let paramType = param["type"] as! String
                if paramType == "text" {
                    let paramValue = param["value"] as! String
                    body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                    let paramSrc = param["src"] as! String
                    let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                    let fileContent = String(data: fileData!, encoding: .utf8)!
                    body += "; filename=\"\(paramSrc)\"\r\n"
                        + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
            }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        var request = URLRequest(url: URL(string: "https://7tracking.com/api/api.php")!,timeoutInterval: Double.infinity)
        request.addValue("r", forHTTPHeaderField: "Cookie")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          
            
          print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async{

            do {
                // make sure this JSON is in the format we expect
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    print(json)
                    let message = json["message"] as? String
                    let result = json["result"] as? Int
                    if((message?.lowercased().elementsEqual("registered successfully!"))! && result == 1){
                        alert.dismiss(animated: true, completion: {
                            UserDefaults.standard.set(self.nameTxtField.text, forKey: "username")
                            UserDefaults.standard.set(self.emailTxtField.text, forKey: "email")
                            UserDefaults.standard.set(json["id"], forKey: "userId")
                            UserDefaults.standard.set(true, forKey: "LoggedIn")
                            self.moveToNextController()
                        })
                        //self.showAlert(title: "Create Account", msg: "Registered Successfully!")
                        
                        
                    }
                }
            } catch let error as NSError {
                alert.dismiss(animated: true, completion: nil)
                self.showAlert(title: "Create Account", msg: "Registered Unsuccessfully!")
                print("Failed to load: \(error.localizedDescription)")
            }
            }
            self.semaphore.signal()
            
        }

        task.resume()
        semaphore.wait()
        
    }
    
    
    @IBAction func backBtnTapped(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController") as? ViewController
             //        self.navigationController?.pushViewController(vc!, animated: true)
             vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
             //        vc!.songDataDict = dict
             //        self.navigationController?.pushViewController(vc!, animated: true)
             self.present(vc!, animated: true, completion: nil)
    }
    func moveToNextController(){
              let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBar_ViewController") as? TabBar_ViewController
          vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        UserDefaults.standard.set(true, forKey: "LoggedIn")
          self.present(vc!, animated: true, completion: nil)
      }
    func verifyAllFields() -> Bool{
        
        if((nameTxtField.text?.trimmingCharacters(in: CharacterSet.whitespaces).elementsEqual(""))! == true){
            
            showAlert(title: "Create Account", msg: "Please enter name.")
        }
        else if((emailTxtField.text?.trimmingCharacters(in: CharacterSet.whitespaces).elementsEqual(""))! == true){
            
            showAlert(title: "Create Account", msg: "Please enter email.")

            
        } else if( (passwordTxtField.text?.trimmingCharacters(in: CharacterSet.whitespaces).elementsEqual(""))! == true){

            showAlert(title: "Create Account", msg: "Please enter password.")
            
        }
        else if((confirmPasswordTxtField.text?.trimmingCharacters(in: CharacterSet.whitespaces).elementsEqual(""))! == true){
            
            showAlert(title: "Create Account", msg: "Please enter confirm password.")
            
        }
        else{
            return true
        }
        
        return false
    }
    
    
    func showAlert(title:String,msg:String){
           let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            self.moveToNextController()
        }))
           self.present(alert, animated: true, completion: nil)
       }
    @IBAction func registerTapped(_ sender: Any) {
        
        let name = nameTxtField.text!
        let email = emailTxtField.text!
        let password = passwordTxtField.text!

        
        
        if(verifyAllFields()){
            registerParamArray = []
            registerParamArray?.append( [
                "key": "type",
                "value": "register",
                "type": "text"
            ])
            registerParamArray?.append( [
                "key": "username",
                "value": name,
                "type": "text"
            ])
            registerParamArray?.append( [
                "key": "email",
                "value": email,
                "type": "text"
            ])
            registerParamArray?.append( [
                "key": "password",
                "value": password,
                "type": "text"
            ])
            
            
            callRegisterService(paramArray: registerParamArray!)
        }
    }
    



}
struct RegisterUser: Codable, CustomStringConvertible {
    var result: String?
    var message: String?
//    var password: String?
//    var total: Int?
//    var totalPages: Int?
    var data: [RegisterUser]?
    
    var description: String {
        var desc = """
        result = \(String(describing: result))
        message = \(String(describing: message))

        
        """
        if let users = data {
            for user in users {
                desc += user.description
            }
        }
        
        return desc
    }
}

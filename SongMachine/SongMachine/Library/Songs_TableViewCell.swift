//
//  Songs_TableViewCell.swift
//  SongMachine
//
//  Created by SAM on 19/10/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

class Songs_TableViewCell: UITableViewCell {

    @IBOutlet var detail: UILabel!
    @IBOutlet var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

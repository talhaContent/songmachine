//
//  Songs_Library_ViewController.swift
//  SongMachine
//
//  Created by SAM on 13/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

class Songs_Library_ViewController: UIViewController,UIPopoverPresentationControllerDelegate {

    var semaphore = DispatchSemaphore (value: 0)
    var totalSongs: Array<Any>?
    var songDict: Array<Dictionary<String, Any>>?
    var songDataDict: Dictionary<String, Any> = ["":""]
    @IBOutlet weak var searchBar: UISearchBar!
    
    var model: [SongModel]!
    var filteredModel = [SongModel]()
    @IBOutlet var mainTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        mainTableView.delegate = self
        mainTableView.dataSource = self
        
        hideKeyboard()
        mainTableView.register(UINib.init(nibName: "LibraryCell_TableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        if(songDict == nil){
        songDict = []
        }
        model = []
        callService()
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    @objc func hideTapped(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    func hideKeyboard(){
        let bar = UIToolbar()
        let reset = UIBarButtonItem(title: "Hide Keyboard", style: .plain, target: self, action: #selector(hideTapped))
        bar.items = [reset]
        bar.sizeToFit()
        searchBar.inputAccessoryView = bar
    }
    func callService(){

        var userid = UserDefaults.standard.string(forKey: "userId")
        if(userid == nil){
            userid = ""
        }
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        
        
        let parameters = [
          [
            "key": "type",
            "value": "all_songs",
            "type": "text"
          ],
          [
            "key": "user_id",
            "value": userid ?? "",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var _: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://7tracking.com/api/api.php")!,timeoutInterval: Double.infinity)
        request.addValue("recent_songs", forHTTPHeaderField: "type")
        request.addValue("1", forHTTPHeaderField: "user_id")
        request.addValue("PHPSESSID=n18qits4o0v908r7c4kusse4d2", forHTTPHeaderField: "Cookie")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async {
                do {
                    // make sure this JSON is in the format we expect
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]{
                        alert.dismiss(animated: true, completion: nil)
                        let data = json["data"] as? Array<Any>
                        self.model = [SongModel]()
                        if let data = data{
                            for jsonResult in data{
                                
                                let jsonDict = jsonResult as! [String:String]
                                let song = SongModel(id: jsonDict["id"], title: jsonDict["title"], genre: jsonDict["genre"], song_path: jsonDict["song_path"], song_key: jsonDict["song_key"], bpm: jsonDict["bpm"], first: jsonDict["first"], second: jsonDict["second"], bsection: jsonDict["bsection"], hook: jsonDict["hook"], bridge: jsonDict["bridge"], datetime: jsonDict["datetime"])
                                self.model.append(song)
                            }
                            self.filteredModel = self.model
                            self.mainTableView.reloadData()
                        }
                    }else{
                        alert.dismiss(animated: true, completion: nil)
                    }
                } catch let error as NSError {
                    alert.dismiss(animated: true, completion: nil)
                    //self.showAlert(title: "Failed to load", msg: "\(error.localizedDescription)")
                    //print("Failed to load: \(error.localizedDescription)")
                }
                
            }
            
            self.semaphore.signal()
        }

        task.resume()
        semaphore.wait()

    }
    
    @IBAction func shareBtn(_ sender: Any) {
        
        let fileUrl1 = NSURL(fileURLWithPath: songDataDict["songPath"] as! String)
        let filename = getDocumentsDirectory().appendingPathComponent(String(fileUrl1.absoluteURL?.lastPathComponent ?? ""))
        let str = String(fileUrl1.absoluteURL?.lastPathComponent ?? "")
            do {
                try str.write(toFile: filename, atomically: true, encoding: String.Encoding.utf8)

                let fileURL = NSURL(fileURLWithPath: filename)

                let objectsToShare = [fileURL]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                self.present(activityVC, animated: true, completion: nil)

            } catch {
                print("cannot write file")
                // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
            }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    func showAlert(title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
           return UIModalPresentationStyle.none
       }

}


extension Songs_Library_ViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LibraryCell_TableViewCell
        if(self.filteredModel.count > 0){
            cell.titleLbl!.text = filteredModel[indexPath.row].title.capitalized
            cell.titleLbl.font = UIFont(name: "Aileron-Regular", size: 18)
            let detail = filteredModel[indexPath.row].genre.capitalized
            cell.detailLbl!.text = detail
            cell.detailLbl.font = UIFont(name: "Aileron-Regular", size: 12)
            cell.SongKeyLbl.text = filteredModel[indexPath.row].song_key.capitalized
            cell.SongKeyLbl.font = UIFont(name: "Aileron-Regular", size: 12)
        }
        return cell
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SongPlayer_ViewController") as? SongPlayer_ViewController
        if(self.filteredModel.count > 0){
            songDataDict["songTitle"] = filteredModel[indexPath.row].title!
            songDataDict["songGenre"] = filteredModel[indexPath.row].genre!
            songDataDict["songPath"] = filteredModel[indexPath.row].song_path
            songDataDict["songStyle"] = filteredModel[indexPath.row].song_key
            songDataDict["songBpm"] = filteredModel[indexPath.row].bpm!
            songDataDict["1st verse_lyrics"] = filteredModel[indexPath.row].first!
            songDataDict["2nd verse_lyrics"] = filteredModel[indexPath.row].second!
            songDataDict["b section_lyrics"] = filteredModel[indexPath.row].bsection!
            songDataDict["hook_lyrics"] = filteredModel[indexPath.row].hook!
            songDataDict["bridge_lyrics"] = filteredModel[indexPath.row].bridge!
            
            
            vc!.songDataDict = songDataDict
            vc?.model = model
            let fileUrl = NSURL(fileURLWithPath: songDataDict["songPath"] as! String)
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
            _ = documentsPath as String + "/" + String(fileUrl.absoluteURL?.lastPathComponent ?? "")
            let url = NSURL(fileURLWithPath: documentsPath as String + "/" + String(fileUrl.absoluteURL?.lastPathComponent ?? ""))
            vc!.mergeAudioURL = url
            vc!.songDict = songDict
            vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
            self.present(vc!, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80
    }
    
}
extension Songs_Library_ViewController : UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText == ""){
            self.filteredModel = model
            
        }else{
            let filtered = model.filter { $0.title.lowercased().contains(searchText.lowercased())}
            self.filteredModel = filtered
            
        }
        self.mainTableView.reloadData()
        
//
    }
}

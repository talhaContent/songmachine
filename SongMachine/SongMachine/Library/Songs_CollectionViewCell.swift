//
//  Songs_CollectionViewCell.swift
//  SongMachine
//
//  Created by SAM on 19/10/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

class Songs_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var title: UILabel!
    @IBOutlet var detail: UILabel!
    
}

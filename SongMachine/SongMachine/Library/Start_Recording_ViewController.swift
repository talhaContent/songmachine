//
//  Start_Recording_ViewController.swift
//  SongMachine
//
//  Created by SAM on 27/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

class Start_Recording_ViewController: UIViewController {

    var semaphore = DispatchSemaphore (value: 0)

    @IBOutlet var libraryTableview: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var logoView: UIView!
    
    @IBOutlet weak var recenTableView: UITableView!
    @IBOutlet weak var recentView: UIView!
    @IBOutlet var libraryView: UIView!
    
    @IBOutlet var ProfileImage: UIImageView!
    var songDict: Array<Dictionary<String, Any>>?
    var songDataDict: Dictionary<String, Any> = ["":""]
    let recentCount = 3
    var recentSongsModel = [SongModel]()
    var filteredRecentSongs = [SongModel]()
    var model: [SongModel]!
    var filteredModel = [SongModel]()
    override func viewWillDisappear(_ animated: Bool) {
        self.recenTableView.reloadData()
        self.libraryTableview.reloadData()
        super.viewWillDisappear(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let s = documentsPath as String
        print(s)
        
        hideKeyboard()
        self.libraryTableview.register(UINib.init(nibName: "LibraryCell_TableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        self.recenTableView.register(UINib.init(nibName: "LibraryCell_TableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")

        if(songDict == nil){
        songDict = []
        }
        model = []
        callService()
    }
//    func hideKeyboard(){
//        let bar = UIToolbar()
//        let reset = UIBarButtonItem(title: "Hide Keyboard", style: .plain, target: self, action: #selector(hideTapped))
//        bar.items = [reset, next]
//        bar.sizeToFit()
//        self.searchBar.keyb
//
//    }
    func hideKeyboard(){
        let bar = UIToolbar()
        let reset = UIBarButtonItem(title: "Hide Keyboard", style: .plain, target: self, action: #selector(hideTapped))
        bar.items = [reset]
        bar.sizeToFit()
        searchBar.inputAccessoryView = bar
        
        
    }
    @objc func hideTapped(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    @objc func goToSetting(){
        self.tabBarController?.selectedIndex = 4
    }
    override func viewWillAppear(_ animated: Bool) {
        self.libraryTableview.reloadData()
        self.recenTableView.reloadData()
        
        super.viewWillAppear(true)
    }
    @objc func closeKeyboard(){
        self.searchBar.endEditing(true)
    }
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    func callService(){

        var userid = UserDefaults.standard.string(forKey: "userId")
        if(userid == nil){
            userid = ""
        }
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        //present(alert, animated: true, completion: nil)
        
        
        
        let parameters = [
          [
            "key": "type",
            "value": "all_songs",
            "type": "text"
          ],
          [
            "key": "user_id",
            "value": userid ?? "",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var _: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://7tracking.com/api/api.php")!,timeoutInterval: Double.infinity)
        request.addValue("recent_songs", forHTTPHeaderField: "type")
        request.addValue("1", forHTTPHeaderField: "user_id")
        request.addValue("PHPSESSID=n18qits4o0v908r7c4kusse4d2", forHTTPHeaderField: "Cookie")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async {
                
                do {
                    // make sure this JSON is in the format we expect
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        print(json)
                        let data = json["data"] as? Array<Any>
                        self.model = [SongModel]()
                        
                        if let data = data{
                            for jsonResult in data{
                                
                                let jsonDict = jsonResult as! [String:String]
                                let song = SongModel(id: jsonDict["id"], title: jsonDict["title"], genre: jsonDict["genre"], song_path: jsonDict["song_path"], song_key: jsonDict["song_key"], bpm: jsonDict["bpm"], first: jsonDict["first"], second: jsonDict["second"], bsection: jsonDict["bsection"], hook: jsonDict["hook"], bridge: jsonDict["bridge"], datetime: jsonDict["datetime"])
                                self.model.append(song)
                            }
                            if(self.model.count > self.recentCount){
                                for n in 0...self.recentCount {
                                    self.recentSongsModel.append(self.model[n])
                                }
                            }else{
                                for n in 0...self.model.count - 1 {
                                    self.recentSongsModel.append(self.model[n])
                                }
                            }
                            
                            self.filteredRecentSongs = self.recentSongsModel
                            self.filteredModel = self.model
                            self.libraryTableview.reloadData()
                            self.recenTableView.reloadData()
                        }
                        
                    }
                } catch let error as NSError {
                    alert.dismiss(animated: true, completion: nil)
                    self.showAlert(title: "Failed to load", msg: "\(error.localizedDescription)")
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            
            
            
            
            self.semaphore.signal()
        }

        task.resume()
        semaphore.wait()

    }
    func showAlert(title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showRecentSongs(){
        
    }
    func showAllSongs(){
        
    }
    
    @IBAction func startTapped(_ sender: Any) {
//        let tab = self.presentingViewController as! UITabBarController
//        self.dismiss(animated: true, completion:{
//             tab.selectedIndex = 2
//        })
    }
    
    @IBAction func profileImageTapped(_ sender: Any) {
        self.tabBarController?.selectedIndex = 4
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
}


struct SongModel{
    var id: String!
    var title: String!
    var genre: String!
    var song_path: String!
    var song_key: String!
    var bpm: String!
    var first: String!
    var second: String!
    var bsection: String!
    var hook: String!
    var bridge: String!
    var datetime: String!

}

extension Start_Recording_ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == self.libraryTableview){
            if(filteredModel.count > 0){
                self.libraryView.isHidden = true
                return filteredModel.count
            }else{
                self.libraryView.isHidden = false
                return 0
            }
        }else{
            if(filteredRecentSongs.count > 0 ){
                self.recentView.isHidden = true
                return filteredRecentSongs.count
            }else{
                self.recentView.isHidden = false
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LibraryCell_TableViewCell
        if(tableView == self.libraryTableview){
            if(model.count > 0 && model != nil){
                cell.titleLbl?.text = filteredModel[indexPath.row].title.capitalized
                cell.titleLbl.font = UIFont(name: "Aileron-Regular", size: 18)
                let detail = filteredModel[indexPath.row].genre.capitalized
                cell.detailLbl.font = UIFont(name: "Aileron-Regular", size: 12)
                cell.detailLbl?.text = detail
                cell.SongKeyLbl.text = filteredModel[indexPath.row].song_key.capitalized
                cell.SongKeyLbl.font = UIFont(name: "Aileron-Regular", size: 12)
            }
        }else{
            if(recentSongsModel.count > 0 ){
                cell.titleLbl?.text = filteredRecentSongs[indexPath.row].title.capitalized
                cell.titleLbl.font = UIFont(name: "Aileron-Regular", size: 18)
                let detail = filteredRecentSongs[indexPath.row].genre.capitalized
                cell.detailLbl.font = UIFont(name: "Aileron-Regular", size: 12)
                cell.detailLbl?.text = detail
                cell.SongKeyLbl.text = filteredRecentSongs[indexPath.row].song_key.capitalized
                cell.SongKeyLbl.font = UIFont(name: "Aileron-Regular", size: 12)
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SongPlayer_ViewController") as? SongPlayer_ViewController
              //        self.navigationController?.pushViewController(vc!, animated: true)
        if(tableView == self.libraryTableview){
            if(self.filteredModel.count > 0){
    //        for index in model!{
                songDataDict["songTitle"] = filteredModel[indexPath.row].title!
                songDataDict["songGenre"] = filteredModel[indexPath.row].genre!
                songDataDict["songPath"] = filteredModel[indexPath.row].song_path
                songDataDict["songStyle"] = filteredModel[indexPath.row].song_key
                songDataDict["songBpm"] = filteredModel[indexPath.row].bpm!
                songDataDict["1st verse_lyrics"] = filteredModel[indexPath.row].first!
                songDataDict["2nd verse_lyrics"] = filteredModel[indexPath.row].second!
                songDataDict["b section_lyrics"] = filteredModel[indexPath.row].bsection!
                songDataDict["hook_lyrics"] = filteredModel[indexPath.row].hook!
                songDataDict["bridge_lyrics"] = filteredModel[indexPath.row].bridge!

                vc!.songDataDict = songDataDict
                vc?.model = model
                let fileUrl = NSURL(fileURLWithPath: songDataDict["songPath"] as! String)
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
                _ = documentsPath as String + "/" + String(fileUrl.absoluteURL?.lastPathComponent ?? "")
                let url = NSURL(fileURLWithPath: documentsPath as String + "/" + String(fileUrl.absoluteURL?.lastPathComponent ?? ""))
                vc!.mergeAudioURL = url
                vc!.songDict = songDict
                vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(vc!, animated: true, completion: nil)
            }
        }else{
            if(self.recentSongsModel.count > 0){
                songDataDict["songTitle"] = filteredRecentSongs[indexPath.row].title!
                songDataDict["songGenre"] = filteredRecentSongs[indexPath.row].genre!
                songDataDict["songPath"] = filteredRecentSongs[indexPath.row].song_path
                songDataDict["songStyle"] = filteredRecentSongs[indexPath.row].song_key
                songDataDict["songBpm"] = filteredRecentSongs[indexPath.row].bpm!
                songDataDict["1st verse_lyrics"] = filteredRecentSongs[indexPath.row].first!
                songDataDict["2nd verse_lyrics"] = filteredRecentSongs[indexPath.row].second!
                songDataDict["b section_lyrics"] = filteredRecentSongs[indexPath.row].bsection!
                songDataDict["hook_lyrics"] = filteredRecentSongs[indexPath.row].hook!
                songDataDict["bridge_lyrics"] = filteredRecentSongs[indexPath.row].bridge!

            
    //            let titleTxt = model[indexPath.row].title!
    //            if(titleTxt == title){
                vc!.songDataDict = songDataDict
                
                let fileUrl = NSURL(fileURLWithPath: songDataDict["songPath"] as! String)
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
                _ = documentsPath as String + "/" + String(fileUrl.absoluteURL?.lastPathComponent ?? "")
                let url = NSURL(fileURLWithPath: documentsPath as String + "/" + String(fileUrl.absoluteURL?.lastPathComponent ?? ""))
                vc!.mergeAudioURL = url
                vc?.model = self.model
    //            vc!.mergeAudioURL = songDataDict["songPath"] as! NSURL
                vc!.songDict = songDict
                vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(vc!, animated: true, completion: nil)
            }
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
}

extension Start_Recording_ViewController : UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText == ""){
            filteredModel = model
            filteredRecentSongs = recentSongsModel
        }else{
            let filtered = model.filter { $0.title.lowercased().contains(searchText.lowercased())}
            filteredModel = filtered
            
            let filteredRecent = self.recentSongsModel.filter { $0.title.lowercased().contains(searchText.lowercased())}
            self.filteredRecentSongs = filteredRecent
        }
        self.libraryTableview.reloadData()
        self.recenTableView.reloadData()
//
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}


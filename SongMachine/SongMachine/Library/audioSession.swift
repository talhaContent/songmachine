//
//  audioSession.swift
//  SongMachine
//
//  Created by Technoventive on 15/12/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

let audioSession = AVAudioSession.sharedInstance()

func configureAudioSessionCategory() {
  print("Configuring audio session")
  do {
    try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.voiceChat)
    try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.none)
    print("AVAudio Session out options: ", audioSession.currentRoute)
    print("Successfully configured audio session.")
  } catch (let error) {
    print("Error while configuring audio session: \(error)")
  }
}

func configureAudioSessionToSpeaker(){
    do {
        try audioSession.setCategory(AVAudioSession.Category.playAndRecord)
        try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        try audioSession.setActive(true)
        print("Successfully configured audio session (SPEAKER-Bottom).", "\nCurrent audio route: ",audioSession.currentRoute.outputs)
    } catch let error as NSError {
        print("#configureAudioSessionToSpeaker Error \(error.localizedDescription)")
    }
}

func configureAudioSessionToEarSpeaker(){

    let audioSession:AVAudioSession = AVAudioSession.sharedInstance()
    do { ///Audio Session: Set on Speaker
        try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.none)
        try audioSession.setActive(true)

        print("Successfully configured audio session (EAR-Speaker).", "\nCurrent audio route: ",audioSession.currentRoute.outputs)
    }
    catch{
        print("#configureAudioSessionToEarSpeaker Error \(error.localizedDescription)")
    }
}

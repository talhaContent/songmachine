//
//  LibraryCell_TableViewCell.swift
//  SongMachine
//
//  Created by SAM on 27/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

class RecentCell_TableViewCell: UITableViewCell {

    @IBOutlet var detailLbl: UILabel!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet weak var SongKeyLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  TabBar_ViewController.swift
//  SongMachine
//
//  Created by SAM on 13/09/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

class TabBar_ViewController: UITabBarController {

    enum CardState {
         case expanded
         case collapsed
     }
    
    var songDict: Array<Dictionary<String, Any>>?
    var selectedTabbar:Int = 0
    
    var homeViewController: Start_Recording_ViewController!
    var secondViewController: Songs_Library_ViewController!
    var actionViewController: Write_Song_ViewController!
    var thirdViewController: DictionaryFeature_ViewController!
    var fourthViewController: Settings_ViewController!
    

     
//     var cardViewController:CardViewController!
     var visualEffectView:UIVisualEffectView!
     
     let cardHeight:CGFloat = 600
     let cardHandleAreaHeight:CGFloat = 65
     
     var cardVisible = false
     var nextState:CardState {
         return cardVisible ? .collapsed : .expanded
     }
     
     var runningAnimations = [UIViewPropertyAnimator]()
     var animationProgressWhenInterrupted:CGFloat = 0

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
     
        homeViewController = Start_Recording_ViewController()
        secondViewController = Songs_Library_ViewController()
        actionViewController = Write_Song_ViewController()
        thirdViewController = DictionaryFeature_ViewController()
        fourthViewController = Settings_ViewController()
        
//        for item in self.tabBar.items! as [UITabBarItem] {
//            // loop through all of your elements in TabBar
//            if (item.tag == 1) {
//                item.image = UIImage(named: "setting")
//                item.selectedImage = UIImage(named: "setting")
//
//            }
//        }
//        for item in self.tabBar.items! as [UITabBarItem] {
//                // loop through all of your elements in TabBar
//                if (item.tag == 4) {
//                    item.image = UIImage(named: "setting")
//                    item.selectedImage = UIImage(named: "setting")
//
//                }
//            }
//        for item in self.tabBar.items! as [UITabBarItem] {
//                // loop through all of your elements in TabBar
//                if (item.tag == 4) {
//                    item.image = UIImage(named: "setting")
//                    item.selectedImage = UIImage(named: "setting")
//
//                }
//            }
        for item in self.tabBar.items! as [UITabBarItem] {
                // loop through all of your elements in TabBar
            if (item.tag == 0) {
                item.image = UIImage(named: "dashboard")?.withRenderingMode(.alwaysOriginal)
                item.selectedImage = UIImage(named: "selected_dashboard")?.withRenderingMode(.alwaysOriginal)
                
            }
            else if (item.tag == 1) {
                item.image = UIImage(named: "library")?.withRenderingMode(.alwaysOriginal)
                item.selectedImage = UIImage(named: "selected_library2")?.withRenderingMode(.alwaysOriginal)
                
            }
            else if (item.tag == 2) {
                item.image = UIImage(named: "writesong")?.withRenderingMode(.alwaysOriginal)
                item.selectedImage = UIImage(named: "writesong")?.withRenderingMode(.alwaysOriginal)
                
            }

            else if (item.tag == 3) {
                item.image = UIImage(named: "feature")?.withRenderingMode(.alwaysOriginal)
                item.selectedImage = UIImage(named: "selected_library")?.withRenderingMode(.alwaysOriginal)
                
            }

            else if (item.tag == 4) {
                item.image = UIImage(named: "setting")?.withRenderingMode(.alwaysOriginal)
                item.selectedImage = UIImage(named: "selected_setting")?.withRenderingMode(.alwaysOriginal)
                
            }

            }
//        homeViewController.tabBarItem.image = UIImage(named: "dashboard")?.withRenderingMode(.alwaysOriginal)
//        homeViewController.tabBarItem.selectedImage = UIImage(named: "dashboard")?.withRenderingMode(.alwaysOriginal)
//
//        //homeViewController.tabBarItem.selectedImage =
//        //UIImage(named: "home-selected")
//        secondViewController.tabBarItem.image = UIImage(named: "library")?.withRenderingMode(.alwaysOriginal)
//        secondViewController.tabBarItem.selectedImage = UIImage(named: "selected_library")?.withRenderingMode(.alwaysOriginal)
//        actionViewController.tabBarItem.image = UIImage(named: "writesong")?.withRenderingMode(.alwaysOriginal)
//        actionViewController.tabBarItem.selectedImage = UIImage(named: "selected_writesong")?.withRenderingMode(.alwaysOriginal)
//        thirdViewController.tabBarItem.image = UIImage(named: "feature")?.withRenderingMode(.alwaysOriginal)
//        thirdViewController.tabBarItem.selectedImage = UIImage(named: "selected_feature")?.withRenderingMode(.alwaysOriginal)
//        fourthViewController.tabBarItem.image = UIImage(named: "setting")?.withRenderingMode(.alwaysOriginal)
//        fourthViewController.tabBarItem.selectedImage = UIImage(named: "selected_setting")?.withRenderingMode(.alwaysOriginal)
//
//
        
//        self.tabBar.selectedItem = tabBar.items![selectedTabbar] as! UITabBarItem
        self.tabBarController?.selectedIndex = selectedTabbar
//        if(selectedTabbar == 4){
////            let storyboard = UIStoryboard(name: "Main", bundle: nil)
////            let pvc = storyboard.instantiateViewController(withIdentifier: "Settings_ViewController") as! Settings_ViewController
////            pvc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
////            self.present(pvc, animated: true, completion: nil)
//            self.tabBarController?.selectedIndex = selectedTabbar
//        }
        //self.tabBar.selectedItem = tabBar.items![selectedTabbar]

       // self.tabBarController?.selectedViewController = tabBarController!.viewControllers![selectedTabbar]
        //[tabBar setSelectedItem:[tabBar.items objectAtIndex:item.tag]];

        
    }
    
    
    func setupCard() {
          visualEffectView = UIVisualEffectView()
          visualEffectView.frame = self.view.frame
          self.view.addSubview(visualEffectView)
          
          actionViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Write_Song_ViewController") as? Write_Song_ViewController
          self.addChild(actionViewController)
          self.view.addSubview(actionViewController.view)
          
        actionViewController.view.frame = CGRect(x: self.tabBar.frame.origin.x, y: self.view.frame.height - cardHandleAreaHeight, width: self.view.bounds.width, height: cardHeight)
          
          actionViewController.view.clipsToBounds = true
//          
//          let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleCardTap()))
          let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handleCardPan(recognizer:)))
          
//          actionViewController.view.addGestureRecognizer(tapGestureRecognizer)
//          actionViewController.view.addGestureRecognizer(panGestureRecognizer)
          
          
      }
    
    @objc
     func handleCardTap() {
//         switch recognzier.state {
//         case .ended:
             animateTransitionIfNeeded(state: nextState, duration: 0.9)
//         default:
//             break
//         }
     }
     
     @objc
     func handleCardPan (recognizer:UIPanGestureRecognizer) {
         switch recognizer.state {
         case .began:
             startInteractiveTransition(state: nextState, duration: 0.9)
         case .changed:
             let translation = recognizer.translation(in: self.actionViewController.view)
             var fractionComplete = translation.y / cardHeight
             fractionComplete = cardVisible ? fractionComplete : -fractionComplete
             updateInteractiveTransition(fractionCompleted: fractionComplete)
         case .ended:
             continueInteractiveTransition()
         default:
             break
         }
         
     }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
     func animateTransitionIfNeeded (state:CardState, duration:TimeInterval) {
         if runningAnimations.isEmpty {
             let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                 switch state {
                 case .expanded:
                     self.actionViewController.view.frame.origin.y = self.view.frame.height - self.cardHeight
                 case .collapsed:
                     self.actionViewController.view.frame.origin.y = self.view.frame.height - self.cardHandleAreaHeight
                 }
             }
             
             frameAnimator.addCompletion { _ in
                 self.cardVisible = !self.cardVisible
                 self.runningAnimations.removeAll()
             }
             
             frameAnimator.startAnimation()
             runningAnimations.append(frameAnimator)
             
             
             let cornerRadiusAnimator = UIViewPropertyAnimator(duration: duration, curve: .linear) {
                 switch state {
                 case .expanded:
                     self.actionViewController.view.layer.cornerRadius = 12
                 case .collapsed:
                     self.actionViewController.view.layer.cornerRadius = 0
                 }
             }
             
             cornerRadiusAnimator.startAnimation()
             runningAnimations.append(cornerRadiusAnimator)
             
             let blurAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                 switch state {
                 case .expanded:
                     self.visualEffectView.effect = UIBlurEffect(style: .dark)
                 case .collapsed:
                     self.visualEffectView.effect = nil
                 }
             }
             
             blurAnimator.startAnimation()
             runningAnimations.append(blurAnimator)
             
         }
     }
     
     func startInteractiveTransition(state:CardState, duration:TimeInterval) {
         if runningAnimations.isEmpty {
             animateTransitionIfNeeded(state: state, duration: duration)
         }
         for animator in runningAnimations {
             animator.pauseAnimation()
             animationProgressWhenInterrupted = animator.fractionComplete
         }
     }
     
     func updateInteractiveTransition(fractionCompleted:CGFloat) {
         for animator in runningAnimations {
             animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
         }
     }
     
     func continueInteractiveTransition (){
         for animator in runningAnimations {
             animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
         }
     }
     
    
    
    

}

extension TabBar_ViewController: UITabBarControllerDelegate{
    

    func tabBarController(_ tabBarController: UITabBarController, willBeginCustomizing viewControllers: [UIViewController]) {
        if(tabBarController.selectedIndex == 1){
        if(songDict == nil){
            songDict = []
        }
        }
        
        
    }
    
//    @objc func dismissKeyboard(){
//        self.view.endEditing(true)
//    }
//
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        if(tabBar.selectedItem!.tag == 0){
//            tabBar.selectedItem?.selectedImage = UIImage(named: "dashboard")?.withRenderingMode(.alwaysOriginal)
//        }
//        if((tabBar.selectedItem?.tag) != nil){
//            print("selected ite,")
//        }
        if(item.tag == 2){
            setupCard()
            handleCardTap()
              print("selected ite,")
        }
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {

        
        if(selectedTabbar == 1){
            self.tabBarController?.selectedViewController = Songs_Library_ViewController()
        }
        if(selectedTabbar == 2){
//            self.tabBarController?.selectedViewController = Songs_Library_ViewController()
        }
        return true
    }
    
//    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        if(item.tag == 0){
//
//        }
//    }
    
   
}

//
//  Settings_ViewController.swift
//  SongMachine
//
//  Created by SAM on 18/10/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import UIKit

class Settings_ViewController: UIViewController {

    @IBOutlet weak var settingsTable: UITableView!
    @IBOutlet weak var appVersionLbl: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var username: UILabel!

    @IBOutlet var editView: UIView!
    @IBOutlet var passowrdView: UIView!
    @IBOutlet var rateView: UIView!
    @IBOutlet var logoutView: UIView!
    let iconsStrings = ["Edit_Profile.png","Change_Password.png","RateUs","Logout"]
    let settingsOptions = ["Edit Profile","Change Password","Rate Us","Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.appVersionLbl.text = "App Version: \(version)"
        }
        let usernameTxt = UserDefaults.standard.string(forKey: "username")
        let emailTxt = UserDefaults.standard.string(forKey: "email")
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(moveToEditProfileVC))
        
        editView.addGestureRecognizer(tapGesture)
        let tapGesturePassword = UITapGestureRecognizer(target: self, action: #selector(moveToChangePassword))
        
        passowrdView.addGestureRecognizer(tapGesturePassword)
        let tapGestureLogout = UITapGestureRecognizer(target: self, action: #selector(logoutTapped))
        logoutView.addGestureRecognizer(tapGestureLogout)
       
        username.text = usernameTxt
        email.text = emailTxt
        self.settingsTable.tableFooterView = UIView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.settingsTable.reloadData()
        super.viewWillAppear(true)
    }
    @objc func logoutTapped(){
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                UserDefaults.standard.set(false, forKey: "LoggedIn")
                UserDefaults.standard.set("", forKey: "userId")
                self.present(vc!, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
    }
//    func rateApp() {
//        if #available(iOS 10.3, *) {
//            SKStoreReviewController.requestReview()
//
//        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "appId") {
//            if #available(iOS 10, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }
//    }
    
    @objc func moveToChangePassword(){
          let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChangePassword_ViewController") as? ChangePassword_ViewController
          vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
          self.present(vc!, animated: true, completion: nil)
      }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    @objc func moveToEditProfileVC(){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditProfile_ViewController") as? EditProfile_ViewController
        vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(vc!, animated: true, completion: nil)
    }
}
extension Settings_ViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.iconsStrings.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0){
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditProfile_ViewController") as? EditProfile_ViewController
            vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
            self.present(vc!, animated: true, completion: nil)
        }else if(indexPath.row == 1){
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChangePassword_ViewController") as? ChangePassword_ViewController
            vc!.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(vc!, animated: true, completion: nil)
        }else if(indexPath.row == 2){
            
        }else if(indexPath.row == 3){
            logoutTapped()
        }
        else if(indexPath.row == 4){
            
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell") as! SettingTableViewCell
        
        cell.iconImageView.image = UIImage(imageLiteralResourceName: iconsStrings[indexPath.row])
        cell.optionLbl.text = settingsOptions[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    
}

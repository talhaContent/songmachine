//
//  SettingsTableViewCell.swift
//  SongMachine
//
//  Created by Technoventive on 08/12/2020.
//  Copyright © 2020 SAM. All rights reserved.
//

import Foundation
import UIKit

class SettingTableViewCell : UITableViewCell{
    
    @IBOutlet weak var optionLbl: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
}
